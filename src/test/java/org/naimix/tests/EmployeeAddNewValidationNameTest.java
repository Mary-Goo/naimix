package org.naimix.tests;

import io.qameta.allure.Step;
import io.qameta.allure.Story;
import models.Employee;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import service.RandomGenerate;
import steps.*;

@Story(value = "T649 - WebADMСайта/Компании/Карточка компании/Сотрудники/Добавить сотрудника - Валидация")
public class EmployeeAddNewValidationNameTest {
    private LoginSteps loginSteps = new LoginSteps();
    private CompaniesSteps companiesSteps = new CompaniesSteps();
    private SitebarSteps sitebarSteps = new SitebarSteps();
    private CompanySteps companySteps = new CompanySteps();
    private EmployeeNewSteps employeeNewSteps = new EmployeeNewSteps();
    private CompanyEmployeesSteps companyEmployeesSteps = new CompanyEmployeesSteps();
    private ToastsSteps toastsSteps = new ToastsSteps();
    private RandomGenerate randomGenerate = new RandomGenerate();
    private Employee employee;
    private int countOldEmployee;

    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        employee = new Employee().setAllFills();
        loginSteps.openTest();
        loginSteps.loginForAdmin("web.adminCompanyLogin", "web.adminCompanyPassword");
        sitebarSteps.openCompaniesPage();
        companiesSteps.choseAnyCompany();
        companySteps.openEmployeesPage();
        countOldEmployee = companyEmployeesSteps.findEmployeesNumberByEmailOnAllPages(employee.getEmail());
        companyEmployeesSteps.addNewEmployee();
        employeeNewSteps.fillEmployeeNew(employee);
    }

    @DataProvider
    public Object[][] testData() {
        return new Object[][]{
                {"firstName", randomGenerate.randomString(51), "Не более 50 символов"},
                {"lastName", randomGenerate.randomString(51), "Не более 50 символов"},
                {"patronymic", randomGenerate.randomString(51), "Не более 50 символов"},
        };
    }

    @Test(priority = 1, dataProvider = "testData", description = "Проверка негативного сценария. Поля заполнены свыше 50 символов")
    @Step("Получение ошибки '{error}', при заполнении поля '{fieldName}' значением превышающим допустимое кол-во символов")
    public void checkErrorWhenSavingEmployeeWith51Symbols(String fieldName, String nameValue, String error) {
        employeeNewSteps.checkErrorAfterFillField(fieldName, nameValue, error);
        employeeNewSteps.fillEmployeeSomeField(fieldName, randomGenerate.randomString(50));
    }

    @Test(priority = 2, description = "Проверка позитивного сценария. Поля ФИО заполнены 50 символами")
    public void checkSavingEmployeeWith50Symbols() {
        employeeNewSteps.clickAddButton();
        toastsSteps.shouldBeExistAndWaitNotExist("Сотрудник успешно добавлен");
        companyEmployeesSteps.checkEmployeesNumberByEmailEqualToInt(employee.getEmail(), countOldEmployee + 1);
    }
}