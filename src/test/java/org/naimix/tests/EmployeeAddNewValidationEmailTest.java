package org.naimix.tests;

import io.qameta.allure.Step;
import io.qameta.allure.Story;
import models.Employee;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import page.EmployeeNewPage;
import service.RandomGenerate;
import steps.*;

import static com.codeborne.selenide.Selenide.$;

@Story(value = "T649 - WebADMСайта/Компании/Карточка компании/Сотрудники/Добавить сотрудника - Валидация")
public class EmployeeAddNewValidationEmailTest {
    private LoginSteps loginSteps = new LoginSteps();
    private ErrorSteps errorSteps = new ErrorSteps();
    private CompaniesSteps companiesSteps = new CompaniesSteps();
    private SitebarSteps sitebarSteps = new SitebarSteps();
    private CompanySteps companySteps = new CompanySteps();
    private EmployeeNewSteps employeeNewSteps = new EmployeeNewSteps();
    private CompanyEmployeesSteps companyEmployeesSteps = new CompanyEmployeesSteps();
    private RandomGenerate randomGenerate = new RandomGenerate();
    private Employee employee;
    private ToastsSteps toastsSteps = new ToastsSteps();

    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        employee = new Employee().setAllFills().setEmail("");
        loginSteps.openTest();
        loginSteps.loginForAdmin("web.adminCompanyLogin", "web.adminCompanyPassword");
        sitebarSteps.openCompaniesPage();
        companiesSteps.choseAnyCompany();
        companySteps.openEmployeesPage();
        companyEmployeesSteps.addNewEmployee();
        employeeNewSteps.fillEmployeeNew(employee);
    }

    @DataProvider
    public Object[][] emailData() {
        return new Object[][]{
                {"", "Обязательное поле"},
                {"AutoTest@gmailcom", "Введен некорректный email"},
                {"AutoTestgmail.com", "Введен некорректный email"},
                {"@gmail.com", "Введен некорректный email"},
                {"AutoTest@", "Введен некорректный email"},
                {"AutoTest@gmail.com     ", "Введен некорректный email"},
                {"    AutoTest@gmail.com", "Введен некорректный email"},
        };
    }

    @Test(dataProvider = "emailData", priority = 1, description = "Заполнение поля email не валидными значением. Под полем сообщение об ошибке")
    @Step("email = '{email}', текст ошибки = '{error}'")
    public void checkErrorWhenSavingEmployeeWithWrongEmail(String email, String error) {
        employeeNewSteps.fillEmployeeOneField("email", new EmployeeNewPage().onEmployeeNewForm().emailField, email);
        employeeNewSteps.clickAddButton();
        errorSteps.assertThatTextErrorIsExpected("email", $(new EmployeeNewPage().onEmployeeNewForm().emailError).getText(), error);
    }

    @Test(priority = 2, description = "Заполнение поля email значением превышаюшим допустимое кол-во символов. Сообщение об ошибке в верхнем правом углу")
    public void checkErrorWhenSavingEmployeeWithBigEmail() {
        String email = randomGenerate.randomString(256) + "@gmail.com";
        employeeNewSteps.fillEmployeeOnlyEmail(email);
        employeeNewSteps.clickAddButton();
        toastsSteps.shouldBeExist("Неверный формат поля 'E-mail'");
        employeeNewSteps.clickCancelButton();
    }
}