package org.naimix.tests;

import io.qameta.allure.Story;
import models.Employee;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import steps.*;

@Story(value = "T649 - WebADMСайта/Компании/Карточка компании/Сотрудники/Добавить сотрудника - Валидация")
public class EmployeeAddNewValidationPositiveTest {
    private final LoginSteps loginSteps = new LoginSteps();
    private final CompaniesSteps companiesSteps = new CompaniesSteps();
    private final SitebarSteps sitebarSteps = new SitebarSteps();
    private final CompanySteps companySteps = new CompanySteps();
    private final EmployeeNewSteps employeeNewSteps = new EmployeeNewSteps();
    private final CompanyEmployeesSteps companyEmployeesSteps = new CompanyEmployeesSteps();
    private final ToastsSteps toastsSteps = new ToastsSteps();
    private Employee employee;

    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        employee = new Employee().setAllFills();
        loginSteps.openTest();
        loginSteps.loginForAdmin("web.adminCompanyLogin", "web.adminCompanyPassword");
        sitebarSteps.openCompaniesPage();
        companiesSteps.choseAnyCompany();
    }

    @Test(description = "Заполнение ФИО максимально допустимым количеством символов")
    public void checkSavingEmployeeWithMaxFIO() {
        employee = employee.setRandomFirstNameAtLength(50).setRandomPatronymicAtLength(50).setRandomFirstNameAtLength(50);
        companySteps.openEmployeesPage();
        int countOldEmployee = companyEmployeesSteps.findEmployeesNumberByEmailOnAllPages(employee.getEmail());
        companyEmployeesSteps.addNewEmployee();
        employeeNewSteps.fillEmployeeNew(employee);
        employeeNewSteps.clickAddButton();
        toastsSteps.shouldBeExistAndWaitNotExist("Сотрудник успешно добавлен");
        companiesSteps.assertThatCountCompanyEqualToNumber(companyEmployeesSteps.findEmployeesNumberByEmailOnAllPages(employee.getEmail()),countOldEmployee + 1 );
    }
}