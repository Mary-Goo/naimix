package org.naimix.tests;

import io.qameta.allure.Step;
import io.qameta.allure.Story;
import models.Employee;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import page.EmployeeNewPage;
import steps.*;

import static com.codeborne.selenide.Selenide.$;

@Story(value = "T649 - WebADMСайта/Компании/Карточка компании/Сотрудники/Добавить сотрудника - Валидация")
public class EmployeeAddNewValidationSnilsTest {
    private final LoginSteps loginSteps = new LoginSteps();
    private final CompaniesSteps companiesSteps = new CompaniesSteps();
    private final SitebarSteps sitebarSteps = new SitebarSteps();
    private final CompanySteps companySteps = new CompanySteps();
    private final EmployeeNewSteps employeeNewSteps = new EmployeeNewSteps();
    private final ErrorSteps errorSteps = new ErrorSteps();
    private final CompanyEmployeesSteps companyEmployeesSteps = new CompanyEmployeesSteps();
    private Employee employee;

    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        employee = new Employee().setAllFills().setSnils("");
        loginSteps.openTest();
        loginSteps.loginForAdmin("web.adminCompanyLogin", "web.adminCompanyPassword");
        sitebarSteps.openCompaniesPage();
        companiesSteps.choseAnyCompany();
        companySteps.openEmployeesPage();
        companyEmployeesSteps.addNewEmployee();
        employeeNewSteps.fillEmployeeNew(employee);
    }

    @DataProvider
    public Object[][] snilsData() {
        return new Object[][]{
                {"14523652145", "Неправильно введен СНИЛС"},
                {"9745288757", "СНИЛС может состоять только из 11 цифр"},
        };
    }

    @Test(dataProvider = "snilsData", description = "Заполнение поля СНИЛС не валидными значением. Под полем сообщение об ошибке")
    @Step("СНИЛС = '{snils}', текст ошибки = '{error}'")
    public void checkErrorWhenSavingEmployeeWithWrongSnils(String snils, String error) {
        employeeNewSteps.fillEmployeeOneField("Снилс", new EmployeeNewPage().onEmployeeNewForm().snilsField, snils);
        employeeNewSteps.clickAddButton();
        errorSteps.assertThatTextErrorIsExpected("Снилс", $(new EmployeeNewPage().onEmployeeNewForm().snilsError).getText(), error);
    }
}