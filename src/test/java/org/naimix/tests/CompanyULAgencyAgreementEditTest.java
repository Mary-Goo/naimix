package org.naimix.tests;

import io.qameta.allure.Story;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import service.RandomGenerate;
import steps.*;

import java.text.SimpleDateFormat;
import java.util.Date;

import static com.codeborne.selenide.Selenide.sleep;

@Story(value = "T5371 - АДМ Наймикс/Агентский договор/Добавление информации")
public class CompanyULAgencyAgreementEditTest {
    private LoginSteps loginSteps = new LoginSteps();
    private BeforeClassSteps beforeClassSteps = new BeforeClassSteps();
    private CompaniesSteps companiesSteps = new CompaniesSteps();
    private DocumentsSteps documentsSteps = new DocumentsSteps();
    private ToastsSteps toastsSteps = new ToastsSteps();
    private AgencyContractSteps agencyContractSteps = new AgencyContractSteps();
    private CompanySteps companySteps = new CompanySteps();
    private RandomGenerate randomGenerate = new RandomGenerate();
    private String nameCompany;
    String number = randomGenerate.randomNumber(10);
    String date = new SimpleDateFormat("dd.MM.yyyy").format(new Date());

    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        loginSteps.openTest();
        loginSteps.loginForAdmin("web.adminCompanyLogin", "web.adminCompanyPassword");
        sleep(3000);
        nameCompany = beforeClassSteps.choseOrCreateCompanyUL();
        companiesSteps.closeTasksList();
        companiesSteps.fillShotName(nameCompany);
        companiesSteps.clickFindButton();
        companiesSteps.openCompanyAtName(nameCompany);
        companySteps.openDocumentsPage();
        documentsSteps.clickAgencyContractPage();
    }

    @Test(priority = 1, description = "Отмена добавления информации")
    public void checkCancellingEditingInformationInAgencyAgreement() {
        String oldNumber = agencyContractSteps.getContractNumber();
        String oldDate = agencyContractSteps.getContractDate();
        agencyContractSteps.clickEditButton();
        agencyContractSteps.fillInformation(number, date);
        agencyContractSteps.clickCancelButton();
        agencyContractSteps.assertThatEqualTo(agencyContractSteps.getContractDate(), oldDate);
        agencyContractSteps.assertThatEqualTo(agencyContractSteps.getContractNumber(), oldNumber);
    }

    @Test(priority = 2, description = "Успешное добавления информации")
    public void checkEditingInformationInAgencyAgreement() {
        agencyContractSteps.clickEditButton();
        agencyContractSteps.fillInformation(number, date);
        agencyContractSteps.clickAssentButton();
        toastsSteps.shouldBeExistAndWaitNotExist("Информация успешно отредактирована");
        agencyContractSteps.assertThatEqualTo(agencyContractSteps.getContractDate(), date);
        agencyContractSteps.assertThatEqualTo(agencyContractSteps.getContractNumber(), number);
    }
}