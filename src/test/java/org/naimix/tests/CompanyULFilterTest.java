package org.naimix.tests;

import io.qameta.allure.Story;
import models.Employee;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import steps.*;

import java.util.List;

import static com.codeborne.selenide.Selenide.*;

@Story(value = "T5114 - АдминНаймикса/Компании/Фильтр")
public class CompanyULFilterTest {
    private final LoginSteps loginSteps = new LoginSteps();
    private final BeforeClassSteps beforeClassSteps = new BeforeClassSteps();
    private final CompaniesSteps companiesSteps = new CompaniesSteps();
    private final SitebarSteps sitebarSteps = new SitebarSteps();
    private final CompanySteps companySteps = new CompanySteps();
    private Employee employeeNew;
    private String nameCompany;

    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        loginSteps.openTest();
        loginSteps.loginForAdmin("web.adminCompanyLogin", "web.adminCompanyPassword");
        sitebarSteps.openCompaniesPage();
        companiesSteps.closeTasksList();
        sleep(3000);
        nameCompany = beforeClassSteps.findCompamyForTest();
        companiesSteps.openCompanyAtName(nameCompany);
        companySteps.openEmployeesPage();
        employeeNew = beforeClassSteps.findEmployeeForTest(nameCompany);
        sitebarSteps.openCompaniesPage();
    }

    @Test(description = "Поиск компании по полю 'Заказчик'")
    public void checkSearchCompanyByShotName() {
        refresh();
        companiesSteps.findCompanyFromNameButton(nameCompany);
        List<String> сompanysNames = companiesSteps.getCompanyNames();
        сompanysNames.forEach(name -> companiesSteps.checkNameContainsCheckName(name, nameCompany));
    }

    @Test(description = "Поиск компании по полю 'ФИО сотрудника'")
    public void checkSearchCompanyByEmployeeName() {
        refresh();
        beforeClassSteps.companyFilterEmployeeName(employeeNew.getLastName() + " " + employeeNew.getFirstName() + " " + employeeNew.getPatronymic(), true);
        sitebarSteps.openCompaniesPage();
        beforeClassSteps.companyFilterEmployeeName(employeeNew.getLastName(), false);
        sitebarSteps.openCompaniesPage();
    }

    @Test(description = "Поиск компании по полю 'Телефон сотрудника'")
    public void checkSearchCompanyByPhone() {
        refresh();
        companiesSteps.fillPhone(employeeNew.getPhone());
        companiesSteps.clickFindButton();
        companiesSteps.countCompaniesNotEqualToZero();
    }

    @Test(description = "Поиск компании по полю 'e-mail сотрудника'")
    public void checkSearchCompanyByEmail() {
        refresh();
        companiesSteps.fillEmail(employeeNew.getEmail());
        companiesSteps.clickFindButton();
        companiesSteps.countCompaniesNotEqualToZero();
    }
}