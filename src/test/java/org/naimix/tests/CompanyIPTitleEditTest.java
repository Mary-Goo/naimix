package org.naimix.tests;

import io.qameta.allure.Step;
import io.qameta.allure.Story;
import models.CompanyIP;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import service.RandomGenerate;
import steps.*;

@Story("T1740 - Админ Наймикса/Компании/Карточка компании ИП/Информация - Валидация заголовка")
public class CompanyIPTitleEditTest {
    private LoginSteps loginSteps = new LoginSteps();
    private ErrorSteps errorSteps = new ErrorSteps();
    private CompaniesSteps companiesSteps = new CompaniesSteps();
    private CompanySteps companySteps = new CompanySteps();
    private ToastsSteps toastsSteps = new ToastsSteps();
    private CreateSomethingByAdmin createSomething = new CreateSomethingByAdmin();
    private RandomGenerate randomGenerate = new RandomGenerate();
    private CompanyIP companyIP;
    private String oldName;

    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        companyIP = createSomething.createCompanyIP();
        loginSteps.openTest();
        loginSteps.loginForAdmin("web.adminCompanyLogin", "web.adminCompanyPassword");
        companiesSteps.fillShotName(companyIP.getFullNameIP());
        companiesSteps.clickFindButton();
        companiesSteps.openCompanyAtName(companyIP.getFullNameIP());
    }

    @DataProvider
    public Object[][] error() {
        return new Object[][]{
                {"      ", "Обязательное поле"},
                {"Заго", "Минимальная длина строки 5 символов"},
                {randomGenerate.randomString(101), "Максимальная длина - 100 символов"},
                {"1*Р(@", "Допускается ввод цифр, спецсимволов, кириллицы и латиницы"}
        };
    }

    @DataProvider
    public Object[][] success() {
        return new Object[][]{
                {"1Pо24"},
                {randomGenerate.randomString(100)},
                {"ИП " + companyIP.getLastName()},
        };
    }

    @Test(priority = 1, description = "Отмена изменения заголовка")
    public void checkCompanyIPTitleEdit() {
        oldName = companySteps.getTitleCompany();
        companySteps.clickEditButton();
        companySteps.setNewTitleCompany("ИП " + companyIP.getLastName());
        companySteps.clickCancelEditButton();
        companySteps.assertThatNewNameEqualToOld(companySteps.getTitleCompany(), oldName);
    }

    @Test(priority = 2, dataProvider = "error", description = "Сохранение не валидного заголовка. Видна ошибка")
    @Step("При заголовке ='{title}' текст ошибки '{error}'")
    public void checkCompanyIPTitleEditWithError(String title, String error) {
        companySteps.shouldBeVisibleTitleCompany(companyIP.getFullNameIP());
        oldName = companySteps.getTitleCompany();
        companySteps.clickEditButton();
        companySteps.setNewTitleCompany(title);
        companySteps.clickApplyEditButton();
        errorSteps.assertThatTextErrorIsExpected("Заголовок компании", companySteps.getTextError(),error);
        companySteps.clickCancelEditButton();
        companySteps.assertThatNewNameEqualToOld(companySteps.getTitleCompany(), oldName);
    }

    @Test(dataProvider = "success", priority = 3, description = "Изменение заголовка на валидные значения")
    @Step("Изменение заголовка на {title}")
    public void checkCompanyIPTitleEditAndSave(String title) {
        companySteps.clickEditButton();
        companySteps.setNewTitleCompany(title);
        companySteps.clickApplyEditButton();
        toastsSteps.shouldBeExistAndWaitNotExist("Компания успешно отредактирована");
        companySteps.assertThatNewNameEqualToOld(companySteps.getTitleCompany(), title);
    }
}