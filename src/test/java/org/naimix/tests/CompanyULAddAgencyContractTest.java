package org.naimix.tests;

import io.qameta.allure.Story;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import steps.*;

import java.util.ArrayList;

import static com.codeborne.selenide.Selenide.sleep;

public class CompanyULAddAgencyContractTest {
    private LoginSteps loginSteps = new LoginSteps();
    private BeforeClassSteps beforeClassSteps = new BeforeClassSteps();
    private CompaniesSteps companiesSteps = new CompaniesSteps();
    private DocumentsSteps documentsSteps = new DocumentsSteps();
    private ToastsSteps toastsSteps = new ToastsSteps();
    private AgencyContractSteps agencyContractSteps = new AgencyContractSteps();
    private CompanySteps companySteps = new CompanySteps();
    private String nameCompany;

    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        loginSteps.openTest();
        loginSteps.loginForAdmin("web.adminCompanyLogin", "web.adminCompanyPassword");
        sleep(3000);
        nameCompany = beforeClassSteps.findCompamyForTest();
        companiesSteps.closeTasksList();
        companiesSteps.fillShotName(nameCompany);
        companiesSteps.clickFindButton();
        companiesSteps.openCompanyAtName(nameCompany);
        companySteps.openDocumentsPage();
        documentsSteps.clickAgencyContractPage();
    }

    @Story(value = "T1292 - Админ Наймикса/Карточка компании/Агентский договор - Добавить договор")
    @Test
    public void checkAdditionAgencyContract() {
        agencyContractSteps.clickAddContractButton();
        ArrayList<String> nameFils = new ArrayList<>();
        nameFils.add("test3.png");
        nameFils.add("test1.docx");
        nameFils.add("test2.odt");
        nameFils.add("test.jpg");
        nameFils.forEach(file -> agencyContractSteps.addFile(file));
        agencyContractSteps.shouldHaveSizeInputFiles(nameFils.size());
        agencyContractSteps.deleteAnyAddedFile();
        agencyContractSteps.shouldHaveSizeInputFiles(nameFils.size() - 1);
        agencyContractSteps.clickSaveFileButton();
        toastsSteps.shouldBeExistAndWaitNotExist("Файлы успешно добавлены");
    }
}