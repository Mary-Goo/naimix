package org.naimix.tests;

import io.qameta.allure.Story;
import models.CompanyUL;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import service.RandomInn;
import steps.*;

@Story(value = "T1906 -АдминНаймикса/Компании/Добавить компанию ЮЛ - кнопка 'добавить'")
public class CompanyULAddTest {
    private LoginSteps loginSteps = new LoginSteps();
    private CompanyNewSteps companyNewSteps = new CompanyNewSteps();
    private CompaniesSteps companiesSteps = new CompaniesSteps();
    private SitebarSteps sitebarSteps = new SitebarSteps();
    private RandomInn randomInn = new RandomInn();
    private ToastsSteps toastsSteps = new ToastsSteps();
    private String inn;

    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        inn = randomInn.getInnUl();
        loginSteps.openTest();
        loginSteps.loginForAdmin("web.adminCompanyLogin", "web.adminCompanyPassword");
    }

    @Test(description = "Добавиление компании ЮЛ через кнопку добавить")
    public void checkingAdditionCompanyUL() {
        CompanyUL newCompanyUL = new CompanyUL()
                .setBusinessRegistrationForm("Юридическое лицо")
                .setRandomOfficialCompanyName()
                .setRandomShortCompanyName()
                .setInn(inn)
                .setActualAddress("191036, г Санкт-Петербург, ул 6-я Советская");
        int countOldCompany = companiesSteps.countCompanyFoundByName(newCompanyUL.getShortCompanyName());
        sitebarSteps.openCompaniesPage();
        companiesSteps.closeTasksList();
        companiesSteps.clickcreateCompanyButton();
        companyNewSteps.fillAddCompanyUL(newCompanyUL);
        companyNewSteps.clickApplyButton();
        toastsSteps.shouldBeExist("Компания включена");
        toastsSteps.shouldBeExist("Компания успешно добавлена");
        toastsSteps.waitNotExist("Компания включена");
        toastsSteps.waitNotExist("Компания успешно добавлена");
        companiesSteps.assertThatCountCompanyEqualToNumber(companiesSteps.countCompanyFoundByName(newCompanyUL.getShortCompanyName()),countOldCompany + 1);
    }
}