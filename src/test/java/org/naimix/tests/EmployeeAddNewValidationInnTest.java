package org.naimix.tests;

import io.qameta.allure.Step;
import io.qameta.allure.Story;
import models.Employee;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import page.EmployeeNewPage;
import steps.*;

import static com.codeborne.selenide.Selenide.$;

public class EmployeeAddNewValidationInnTest {
    private LoginSteps loginSteps = new LoginSteps();
    private ErrorSteps errorSteps = new ErrorSteps();
    private CompaniesSteps companiesSteps = new CompaniesSteps();
    private SitebarSteps sitebarSteps = new SitebarSteps();
    private CompanySteps companySteps = new CompanySteps();
    private EmployeeNewSteps employeeNewSteps = new EmployeeNewSteps();
    private CompanyEmployeesSteps companyEmployeesSteps = new CompanyEmployeesSteps();
    private Employee employee;

    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        employee = new Employee().setAllFills().setInnEmployee("");
        loginSteps.openTest();
        loginSteps.loginForAdmin("web.adminCompanyLogin", "web.adminCompanyPassword");
        sitebarSteps.openCompaniesPage();
        companiesSteps.choseAnyCompany();
        companySteps.openEmployeesPage();
        companyEmployeesSteps.addNewEmployee();
        employeeNewSteps.fillEmployeeNew(employee);
    }

    @DataProvider
    public Object[][] innData() {
        return new Object[][]{
                {"852369741258", "Неверный формат ИНН"},
                {"85236974125", "Неверный формат ИНН"},
        };
    }

    @Story(value = "T649 - WebADMСайта/Компании/Карточка компании/Сотрудники/Добавить сотрудника - Валидация")
    @Test(dataProvider = "innData", description = "Заполнение поля ИНН не валидными значением. Под полем сообщение об ошибке")
    @Step("СНИЛС = '{inn}', текст ошибки = '{error}'")
    public void checkErrorWhenSavingEmployeeWithWrongInn(String inn, String error) {
        employeeNewSteps.fillEmployeeOneField("ИНН", new EmployeeNewPage().onEmployeeNewForm().innField, inn);
        employeeNewSteps.clickAddButton();
        errorSteps.assertThatTextErrorIsExpected("ИНН", $(new EmployeeNewPage().onEmployeeNewForm().innError).getText(), error);
    }
}