package org.naimix.tests;

import io.qameta.allure.Story;
import models.CompanyUL;
import models.Employee;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import steps.*;

import static com.codeborne.selenide.Selenide.refresh;

@Story(value = "T581 -WebADMСайта/Компании/Активные - Кнопка в архив")
public class CompanyULArchiveTest {
    private LoginSteps loginSteps = new LoginSteps();
    private CompaniesSteps companiesSteps = new CompaniesSteps();
    private CompanySteps companySteps = new CompanySteps();
    private SitebarSteps sitebarSteps = new SitebarSteps();
    private ToastsSteps toastsSteps = new ToastsSteps();
    private CreateSomethingByAdmin createSomething = new CreateSomethingByAdmin();
    private ModalWindowSteps modalWindowSteps = new ModalWindowSteps();
    private CompanyUL newCompanyUL;
    private Employee newEmployee;

    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        newCompanyUL = createSomething.createCompanyUL();
        newEmployee = createSomething.createEmployee(newCompanyUL.getShortCompanyName());
        loginSteps.openTest();
        loginSteps.loginForAdmin("web.adminCompanyLogin", "web.adminCompanyPassword");
    }

    @Test(priority = 1, description = "Архивация компании. В модальном окне выбрать НЕТ")
    public void checkArchivingCompanyDisagree() {
        companiesSteps.checkNumberOfCompaniesFoundByName(newCompanyUL.getShortCompanyName(), 1);
        companiesSteps.clickArchiveButtonForOneCompanyOnPage();
        modalWindowSteps.shouldBeDisplayedAndClickButton("No");
        refresh();
        companiesSteps.checkNumberOfCompaniesFoundByName(newCompanyUL.getShortCompanyName(), 1); //компания отображается в активных
        companiesSteps.closeTasksList();
        companiesSteps.clickArchiveButton();//переход
        companiesSteps.checkNumberOfCompaniesFoundByName(newCompanyUL.getShortCompanyName(), 0); //компании нет в архиве
        loginSteps.loginOut();
        loginSteps.loginForEmployee(newEmployee.getEmail(), newEmployee.getPassword());//сотрудник компании может залогиниться
        companySteps.shouldBeVisibleTitleCompanyDetails();
        loginSteps.loginOut();
    }

    @Test(priority = 2, description = "Архивация компании. В модальном окне выбрать ДА")
    public void checkArchivingCompanyAgree() {
        loginSteps.openTest();
        loginSteps.loginForAdmin("web.adminCompanyLogin", "web.adminCompanyPassword");
        sitebarSteps.openCompaniesPage();
        companiesSteps.closeTasksList();
        companiesSteps.findCompanyFromNameButton(newCompanyUL.getShortCompanyName());
        companiesSteps.clickArchiveButtonForOneCompanyOnPage();
        modalWindowSteps.shouldBeDisplayedAndClickButton("Yes");
        toastsSteps.shouldBeExistAndWaitNotExist("Компания успешно заархивирована");
        companiesSteps.checkNumberOfCompaniesFoundByName(newCompanyUL.getShortCompanyName(), 0);  //компания нет в активных
        companiesSteps.clickArchiveButton();
        companiesSteps.checkNumberOfCompaniesFoundByName(newCompanyUL.getShortCompanyName(), 1); //компания отображается в архиве
        loginSteps.loginOut();
        loginSteps.loginForEmployee(newEmployee.getEmail(), newEmployee.getPassword());
        toastsSteps.shouldBeExist("Компания в архиве");
    }
}