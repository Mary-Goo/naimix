package org.naimix.tests;

import io.qameta.allure.Story;
import org.testng.annotations.Test;
import service.ConfProperties;
import steps.LoginSteps;

import static com.codeborne.selenide.Selenide.open;

public class LoginTests {

    private LoginSteps loginSteps = new LoginSteps();

    @Test
    @Story(value = "Регистрация на сайте")
    public void checkLogin() {
        open(ConfProperties.getProperty("web.baseUrl"));
        loginSteps.fillLogin(ConfProperties.getProperty("web.adminWebsiteEmail"));
        loginSteps.fillPassword(ConfProperties.getProperty("web.adminWebsitePassword"));
        loginSteps.clickLoginButton();
    }
}