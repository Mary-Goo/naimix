package org.naimix.tests;

import io.qameta.allure.Story;
import models.Employee;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import steps.*;

@Story(value = "T646 -WebADMСайта/Компании/Карточка компании/Сотрудники/Добавить сотрудника - добавление")
public class EmployeeAddNewTest {
    private final LoginSteps loginSteps = new LoginSteps();
    private final SitebarSteps sitebarSteps = new SitebarSteps();
    private final CompanySteps companySteps = new CompanySteps();
    private final CompanyEmployeesSteps companyEmployeesSteps = new CompanyEmployeesSteps();
    private final CompaniesSteps companiesSteps = new CompaniesSteps();
    private final EmployeeNewSteps employeeNewSteps = new EmployeeNewSteps();
    private final ToastsSteps toastsSteps = new ToastsSteps();
    private Employee employeeNew;

    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        employeeNew = new Employee().setAllFills();
        loginSteps.openTest();
        loginSteps.loginForAdmin("web.adminCompanyLogin", "web.adminCompanyPassword");
    }

    @Test(description = "Добавление сотрудника в компанию")
    public void checkAddNewEmployee() {
        sitebarSteps.openCompaniesPage();
        companiesSteps.closeTasksList();
        companiesSteps.choseAnyCompany();
        companySteps.openEmployeesPage();
        int countOldEmployee = companyEmployeesSteps.findEmployeesNumberByEmailOnAllPages(employeeNew.getEmail());
        companyEmployeesSteps.addNewEmployee();
        employeeNewSteps.fillEmployeeNew(employeeNew);
        employeeNewSteps.clickAddButton();
        toastsSteps.shouldBeExistAndWaitNotExist("Сотрудник успешно добавлен");
        companyEmployeesSteps.checkEmployeesNumberByEmailEqualToInt(employeeNew.getEmail(), countOldEmployee + 1);
        loginSteps.loginOut();
        loginSteps.closeCookie();
        loginSteps.loginForEmployee(employeeNew.getEmail(), employeeNew.getPassword());
        companySteps.shouldBeVisibleTitleCompanyDetails();
    }
}