package org.naimix.tests;

import io.qameta.allure.Step;
import io.qameta.allure.Story;
import models.Project;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import steps.LoginSteps;
import steps.ProjectsSteps;
import steps.SitebarAdminCompanySteps;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selenide.sleep;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@Story("T2864 - АдминНаймикс/Проекты/Активные проекты - Проверка фильтров")
public class ProjectFilterTest {
    private LoginSteps loginSteps = new LoginSteps();
    private SitebarAdminCompanySteps sitebarAdminCompanySteps = new SitebarAdminCompanySteps();
    private ProjectsSteps projectsSteps = new ProjectsSteps();
    List<Project> projectsOpen = new ArrayList<>();
    List<Project> projectsClose = new ArrayList<>();
    private Project projectOpen;

    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        loginSteps.openTest();
        loginSteps.loginForAdmin("web.adminWebsiteEmail", "web.adminWebsitePassword");
        sitebarAdminCompanySteps.openProjectPage();
        sleep(300);
        projectOpen = projectsSteps.chooseProjectWithStatus("В работе");
        projectsSteps.chooseProjectWithStatus("Закрыт");
        projectsOpen = projectsSteps.getProjectsOnOpenPage().stream().filter(p -> p.getStatus().equals("В работе")).collect(Collectors.toList());
        projectsClose = projectsSteps.getProjectsOnOpenPage().stream().filter(p -> p.getStatus().equals("Закрыт")).collect(Collectors.toList());
        sitebarAdminCompanySteps.openProjectPage();
    }

    @Test(priority = 1, description = "Проверка фильтра по полю 'Название'")
    public void filterProjectByName() {
        projectsSteps.fillFilterName(projectOpen.getName());
        projectsSteps.clickFindButton();
        sleep(300);
        List<String> findProjects = projectsSteps.getProjectsNameOnOpenPage();
        findProjects.forEach(p -> projectsSteps.checkNameContainsCheckName(p, projectOpen.getName()));
        projectsSteps.clickClearButton();
        sleep(300);
        assertThat(projectsSteps.getProjectsOnOpenPage().size(), equalTo(projectsOpen.size() + projectsClose.size()));
    }

    @DataProvider
    public Object[][] statusData() {
        return new Object[][]{
                {"В работе", projectsOpen},
                {"Закрыт", projectsClose},
        };
    }

    @Test(priority = 2, dataProvider = "statusData", description = "Проверка фильтра по полю 'Статус'")
    @Step("Поиск по статусу '{status}'")
    public void filterProjectByStatus(String status, List<Project> pro) {
        projectsSteps.clickClearButton();
        sleep(300);
        assertThat(projectsSteps.getProjectsOnOpenPage().size(), equalTo(projectsOpen.size() + projectsClose.size()));
        projectsSteps.choseStatusInFilter(status);
        projectsSteps.clickFindButton();
        sleep(300);
        assertThat(projectsSteps.getProjectsOnOpenPage(), equalTo(pro));
    }
}