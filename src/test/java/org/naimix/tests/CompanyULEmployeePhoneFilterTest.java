package org.naimix.tests;

import io.qameta.allure.Step;
import io.qameta.allure.Story;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import page.CompaniesPage;
import steps.CompaniesSteps;
import steps.LoginSteps;
import steps.SitebarSteps;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;

@Story(value = "T5117 - АдминНаймикса/Компании/Поиск по телефону сотрудника")
public class CompanyULEmployeePhoneFilterTest {
    private final LoginSteps loginSteps = new LoginSteps();
    private final CompaniesSteps companiesSteps = new CompaniesSteps();
    private final SitebarSteps sitebarSteps = new SitebarSteps();

    @BeforeClass(alwaysRun = true)
    public void beforeClass() {
        loginSteps.openTest();
        loginSteps.loginForAdmin("web.adminCompanyLogin", "web.adminCompanyPassword");
        sitebarSteps.openCompaniesPage();
        companiesSteps.closeTasksList();
    }

    @DataProvider
    public Object[][] data() {
        return new Object[][]{
                {"Тракторист", "9519519519"},
                {"Тракторист", "1591591591"},
        };
    }

    @Test(dataProvider = "data", description = "Поиск компании по полю 'Телефон сотрудника'")
    @Step("Поиск компании с именем = '{nameCompany}' по номеру телефона '{phone}'")
    public void checkSearchCompanyByPhone(String nameCompany, String phone) {
        companiesSteps.fillPhone(phone);
        companiesSteps.clickFindButton();
        companiesSteps.checkNameEqualToCheckName(companiesSteps.getCompanyName(), nameCompany);
        $(new CompaniesPage().onCompaniesForm().addressCompanyFilter).shouldBe(not(empty));
        $(new CompaniesPage().onCompaniesForm().archiveButtonForOne).shouldBe(visible).shouldHave(exactOwnText("В архив"));
        $(new CompaniesPage().onCompaniesForm().settingsCompany).shouldBe(visible);
    }
}