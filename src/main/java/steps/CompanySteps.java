package steps;

import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;
import page.CompanyInfoPage;
import page.CompanyPage;

import static com.codeborne.selenide.Selenide.$;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class CompanySteps {

    @Step("Открытие вкладки 'Сотрудники'")
    public void openEmployeesPage() {
        $(new CompanyPage().onCompanyForm().employeesPage).click();
    }

    @Step("Открытие вкладки 'Документы'")
    public void openDocumentsPage() {
        $(new CompanyPage().onCompanyForm().documentsPage).click();
    }

    @Step("Получение заголовка компании")
    public String getTitleCompany() {
        return $(new CompanyInfoPage().onCompanyInfoForm().companyTitle).getOwnText();
    }

    @Step("Проверка видимости заголовка компании")
    public void shouldBeVisibleTitleCompany(String title) {
        $(new CompanyInfoPage().onCompanyInfoForm().companyTitle).shouldHave(Condition.textCaseSensitive("ИП " + title));
    }

    @Step("Клик по кнопке 'Редактировать' заголовок компании")
    public void clickEditButton() {
        $(new CompanyInfoPage().onCompanyInfoForm().editButton).click();
    }

    @Step("Ввод нового значения")
    public void setNewTitleCompany(String title) {
        $(new CompanyInfoPage().onCompanyInfoForm().fieldTitleCompany).setValue(title);
    }

    @Step("Отмена редактирования заголовка компании")
    public void clickCancelEditButton() {
        $(new CompanyInfoPage().onCompanyInfoForm().cancelEditButton).click();
    }

    @Step("Сохранение редактирования заголовка компании")
    public void clickApplyEditButton() {
        $(new CompanyInfoPage().onCompanyInfoForm().applyEditButton).click();
    }

    @Step("Текст ошибки")
    public String getTextError() {
        return $(new CompanyInfoPage().onCompanyInfoForm().errorText).getOwnText();
    }

    @Step("Проверка отображения заголовка 'Реквизиты компании'")
    public void shouldBeVisibleTitleCompanyDetails(){
        $(new CompanyInfoPage().onCompanyInfoForm().companyDetailsTitle).shouldBe(Condition.visible);
    }

    @Step("Проверка совподают ли заголовки")
    public void assertThatNewNameEqualToOld(String newName, String shouldName) {
        assertThat(newName, equalTo(shouldName));
    }
}