package steps;

import io.qameta.allure.Step;
import models.Employee;
import org.openqa.selenium.By;
import page.EmployeeNewPage;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class EmployeeNewSteps {

    @Step("Заполнение формы добавления нового сотрудника")
    public void fillEmployeeNew(Employee employeeNew) {
        $(new EmployeeNewPage().onEmployeeNewForm().lastNameField).setValue(employeeNew.getLastName());
        $(new EmployeeNewPage().onEmployeeNewForm().firstNameField).setValue(employeeNew.getFirstName());
        $(new EmployeeNewPage().onEmployeeNewForm().patronymicField).setValue(employeeNew.getPatronymic());
        $(new EmployeeNewPage().onEmployeeNewForm().snilsField).click();
        $(new EmployeeNewPage().onEmployeeNewForm().snilsField).setValue(employeeNew.getSnils());
        $(new EmployeeNewPage().onEmployeeNewForm().innField).click();
        $(new EmployeeNewPage().onEmployeeNewForm().innField).setValue(employeeNew.getInnEmployee());
        $$(new EmployeeNewPage().onEmployeeNewForm().anyDropDown).stream().findAny().get().click();
        $(new EmployeeNewPage().onEmployeeNewForm().phoneField).click();
        $(new EmployeeNewPage().onEmployeeNewForm().phoneField).setValue(employeeNew.getPhone());
        $(new EmployeeNewPage().onEmployeeNewForm().emailField).setValue(employeeNew.getEmail());
        $$(new EmployeeNewPage().onEmployeeNewForm().anyDropDown).stream().findAny().get().click();
        $(new EmployeeNewPage().onEmployeeNewForm().passwordField).setValue(employeeNew.getPassword());
        $(new EmployeeNewPage().onEmployeeNewForm().repeatPasswordField).setValue(employeeNew.getPassword());
    }

    @Step("Заполнение в форме нового сотрудника только поля 'ИНН'")
    public void fillEmployeeOnlyInn(String inn) {
        $(new EmployeeNewPage().onEmployeeNewForm().innField).click();
        $(new EmployeeNewPage().onEmployeeNewForm().innField).setValue(inn);
    }

    @Step("Заполнение в форме нового сотрудника только поля 'СНИЛС'")
    public void fillEmployeeOnlySnils(String snils) {
        $(new EmployeeNewPage().onEmployeeNewForm().snilsField).click();
        $(new EmployeeNewPage().onEmployeeNewForm().snilsField).setValue(snils);
    }

    @Step("Заполнение поля {field} с локатором {locator} значением {value}")
    public void fillEmployeeOneField(String field, By locator, String value) {
        $(locator).click();
        $(locator).setValue(value);
    }

    @Step("Заполнение в форме нового сотрудника только поля 'Телефон'")
    public void fillEmployeeOnlyPhone(String phone) {
        $(new EmployeeNewPage().onEmployeeNewForm().phoneField).click();
        $(new EmployeeNewPage().onEmployeeNewForm().phoneField).setValue(phone);
    }

    @Step("Заполнение в форме нового сотрудника только поля 'Email'")
    public void fillEmployeeOnlyEmail(String email) {
        $(new EmployeeNewPage().onEmployeeNewForm().emailField).click();
        $(new EmployeeNewPage().onEmployeeNewForm().emailField).setValue(email);
    }

    @Step("Заполнение в форме нового сотрудника только поля 'Имя'")
    public void fillEmployeeOnlyFirstname(String name) {
        $(new EmployeeNewPage().onEmployeeNewForm().firstNameField).setValue(name);
    }

    @Step("Заполнение в форме нового сотрудника только поля 'Отчество'")
    public void fillEmployeeOnlyPatronymic(String name) {
        $(new EmployeeNewPage().onEmployeeNewForm().patronymicField).setValue(name);
    }
    @Step("Клик по кнопке 'Добавить'")
    public void clickAddButton() {
        $(new EmployeeNewPage().onEmployeeNewForm().addButton).click();
    }

    @Step("Клик по кнопке 'Отменить'")
    public void clickCancelButton() {
        $(new EmployeeNewPage().onEmployeeNewForm().cancelButton).click();
    }

    @Step("Заполнение в форме нового сотрудника поля '{fieldName}' значением '{name}'")
    public void fillEmployeeSomeField(String fieldName, String name) {
        $(new EmployeeNewPage().onEmployeeNewForm().inputFieldByName(fieldName)).setValue(name);
    }

    @Step("Проверка получения ошибки: {error} при заполнении поля '{fieldName}' значением '{name}'")
    public void checkErrorAfterFillField(String fieldName, String name, String error) {
        fillEmployeeSomeField(fieldName, name);
        clickAddButton();
        assertThat($(new EmployeeNewPage().onEmployeeNewForm().errorTextValueUnderField(fieldName)).getText(), equalTo(error));
    }
}