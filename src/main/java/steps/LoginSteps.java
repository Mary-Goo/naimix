package steps;

import io.qameta.allure.Step;
import page.LoginPage;
import service.ConfProperties;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class LoginSteps {

    @Step("Заполнение поля 'E-mail' - логин")
    public void fillLogin(String login) {
        $(new LoginPage().onLoginForm().loginField).setValue(login);
    }

    @Step("Заполнение поля 'Пароль'")
    public void fillPassword(String password) {
        $(new LoginPage().onLoginForm().passwordField).setValue(password);
    }

    @Step("Клик по кнопке 'Войти'")
    public void clickLoginButton() {
        $(new LoginPage().onLoginForm().loginButton).pressEnter();
    }

    @Step("Открыть тестовый стенд")
    public void openTest() {
        open(ConfProperties.getProperty("web.baseUrl"));
    }

    @Step("Авторизация под админом {login}")
    public void loginForAdmin(String login, String password) {
        $(new LoginPage().onLoginForm().loginField).setValue(ConfProperties.getProperty(login));
        $(new LoginPage().onLoginForm().passwordField).setValue(ConfProperties.getProperty(password));
        $(new LoginPage().onLoginForm().loginButton).pressEnter();
    }

    @Step("Выход")
    public void loginOut() {
        $(new LoginPage().onLoginForm().loginOutButton).click();
    }

    @Step("Закрыть cookie")
    public void closeCookie() {
        $(new LoginPage().onLoginForm().cookie).click();
    }

    @Step("Авторизация под сотрудником {email}")
    public void loginForEmployee(String email, String password) {
        fillLogin(email);
        fillPassword(password);
        clickLoginButton();
    }
}