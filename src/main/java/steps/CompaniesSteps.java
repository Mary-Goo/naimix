package steps;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import models.CompanyUL;
import org.openqa.selenium.By;
import page.CompaniesPage;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Condition.exactOwnText;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.*;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsStringIgnoringCase;
import static org.hamcrest.Matchers.not;

public class CompaniesSteps {

    @Step("Клик по кнопке '+ Создать компанию'")
    public void clickcreateCompanyButton() {
        $(new CompaniesPage().onCompaniesForm().createCompanyButton).click();
    }

    @Step("Выбор случайной компании")
    public void choseAnyCompany() {
        $$((new CompaniesPage().onCompaniesForm().allCompanyNames)).stream().findAny().get().click();
    }

    @Step("Выбор случайной компании. Получение имени")
    public String choseAnyCompanyReturnName() {
        return $$((new CompaniesPage().onCompaniesForm().allCompanyNames)).stream().findAny().get().getText();
    }

    @Step("Переход к данным компании, выбранной по имени")
    public void openCompanyAtName(String name) {
        $$((new CompaniesPage().onCompaniesForm().allCompanyNames)).findBy(text(name)).click();
    }

    @Step("Закрытие сообщения про задачи")
    public void closeTasksList() {
        if ($(new CompaniesPage().onCompaniesForm().tasksList).isDisplayed()) {
            $(new CompaniesPage().onCompaniesForm().tasksList).click();
        }
    }

    @Step("Подсчет колличества найденных компании по полю 'Заказчик' = короткое имя")
    public int countCompanyFoundByName(String shortCompanyName) {
        findCompanyFromNameButton(shortCompanyName);
        return countFindCompany(shortCompanyName);
    }

    @Step("Поиск компании по полю 'Заказчик' = короткое имя")
    public void findCompanyFromNameButton(String shortCompanyName) {
        fillShotName(shortCompanyName);
        clickFindButton();
    }

    @Step("Клик по кнопке 'Архив' для единственной компании на странице")
    public void clickArchiveButtonForOneCompanyOnPage() {
        $$(new CompaniesPage().onCompaniesForm().wholeCompany).shouldHaveSize(1);
        $(new CompaniesPage().onCompaniesForm().archiveButtonForOne).click();
    }

    @Step("Клик по кнопке Архив. Переход Архив->Список / Список->Архив")
    public void clickArchiveButton() {
        $(new CompaniesPage().onCompaniesForm().archiveButton).click();
    }

    @Step("Заполнить поле 'Заказчик'")
    public void fillShotName(String shortCompanyName) {
        $(new CompaniesPage().onCompaniesForm().shortCompanyNameFilter).setValue(shortCompanyName);
    }

    @Step("Заполнить поле 'ФИО сотрудника'")
    public void fillСlientUserFioFilter(String name) {
        $(new CompaniesPage().onCompaniesForm().clientUserFioFilter).setValue(name);
    }

    @Step("Заполнить поле 'Телефон сотрудника' {phone}")
    public void fillPhone(String phone) {
        $(new CompaniesPage().onCompaniesForm().clientUserPhoneFilter).setValue(phone);
    }

    @Step("Заполнить поле 'e-mail сотрудника' {email}")
    public void fillEmail(String email) {
        $(new CompaniesPage().onCompaniesForm().clientUserEmailFilter).setValue(email);
    }

    @Step("Клик по кнопке 'Найти'")
    public void clickFindButton() {
        $(new CompaniesPage().onCompaniesForm().findButton).click();
    }

    @Step("Подсчет кол-ва найденных компаний")
    public int countFindCompany(String shortCompanyName) {
        return (int) $$(new CompaniesPage().onCompaniesForm().allCompanyNames).filterBy(exactOwnText(shortCompanyName)).stream().count();
    }

    @Step("Подсчет кол-ва компаний, отображаемых на странице")
    public int findCompanyFromPageCount() {
        return (int) $$(new CompaniesPage().onCompaniesForm().allCompanyNames).stream().count();
    }

    @Step("Получение имени компании. Компания уникальна")
    public String getCompanyName() {
        $$(new CompaniesPage().onCompaniesForm().allCompanies).shouldHaveSize(1);
        return $(new CompaniesPage().onCompaniesForm().allCompanies).getText();
    }

    @Step("Найдейнные компании")
    public List<CompanyUL> getFindCompanies() {
        List<CompanyUL> companies = new ArrayList<>();
        List<SelenideElement> elements = $$(By.xpath("//tr[@class='middle aligned']"));
        for (SelenideElement element : elements) {
            String shortName = element.find(new CompaniesPage().onCompaniesForm().companyName).getText();
            String address = element.find(new CompaniesPage().onCompaniesForm().addressCompany).getText();
            CompanyUL companyUL = new CompanyUL().setShortCompanyName(shortName).setActualAddress(address);
            companies.add(companyUL);
        }
        return companies;
    }

    @Step("Список имен, найдейнных компаний")
    public List<String> getCompanyNames() {
        List<String> companysNames = new ArrayList<>();
        sleep(3000);
        List<SelenideElement> elements = $$(new CompaniesPage().onCompaniesForm().allCompanyNames);
        for (SelenideElement element : elements) {
            String name = element.getText();
            companysNames.add(name);
        }
        return companysNames;
    }

    @Step("Проверка, что найдено более чем 0 компаний")
    public void countCompaniesNotEqualToZero() {
        assertThat($$(new CompaniesPage().onCompaniesForm().allCompanyNames).stream().count(), not(equalTo(0)));
    }

    @Step("Проверка, что имя найденной компании содержит имя искомой компании")
    public void checkNameContainsCheckName(String name, String checkName) {
        assertThat(name, containsStringIgnoringCase(checkName));
    }

    @Step("Проверка, что имя найденной компании равно имени искомой компании")
    public void checkNameEqualToCheckName(String name, String checkName) {
        assertThat(name, equalTo(checkName));
    }

    @Step("Проверка равенства найденных компаний по имени: {shortCompanyName} числу {number}")
    public void checkNumberOfCompaniesFoundByName(String shortCompanyName, int number) {
        findCompanyFromNameButton(shortCompanyName);
        assertThat(countFindCompany(shortCompanyName), equalTo(number));
    }

    @Step("Проверка равенства компаний на странице числу {number}")
    public void assertThatCountCompanyEqualToNumber(int countCompany, int number) {
        assertThat(countCompany, equalTo(number));
    }
}