package steps;

import io.qameta.allure.Step;
import models.CompanyIP;
import org.openqa.selenium.Keys;
import page.CompanyNewIPPage;
import page.CompanyNewPage;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static org.openqa.selenium.Keys.DELETE;
import static org.openqa.selenium.Keys.chord;

public class CompanyNewIPSteps {
    private CompanyNewSteps companyNewSteps = new CompanyNewSteps();

    @Step("Заполнение формы добавления новой компании 'Индивидуальный предприниматель'")
    public void fillAddCompanyIP(CompanyIP companyIP) {
        companyNewSteps.choseRegistrationForm(companyIP.getBusinessRegistrationForm());
        $(new CompanyNewIPPage().onCompanyNewIPForm().fullNameField).setValue(companyIP.getFullNameIP());
        $(new CompanyNewPage().onCompanyNewForm().innCompanyField).setValue(companyIP.getInn());
        $(new CompanyNewPage().onCompanyNewForm().addressIn).sendKeys(chord(Keys.CONTROL, "a"), DELETE);
        $(new CompanyNewPage().onCompanyNewForm().addressIn).setValue(companyIP.getActualAddress());
        $(new CompanyNewPage().onCompanyNewForm().addressChose).click();
        $$(new CompanyNewPage().onCompanyNewForm().categoryCompanyField).stream().findAny().get().click();
    }
}