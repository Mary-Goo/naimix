package steps;

import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;
import page.ToastsPage;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.not;
import static com.codeborne.selenide.Selenide.$$;

public class ToastsSteps {

    @Step("Видимость уведомлений")
    public void shouldBeExist(String text) {
        $$(new ToastsPage().onToastsForm().toast).findBy(Condition.text(text)).shouldBe(exist);
    }

    @Step("Ожидание, когда успешное уведомление пропадет")
    public void waitNotExist(String text) {
        $$(new ToastsPage().onToastsForm().toast).findBy(Condition.text(text)).waitUntil(not(exist), 10000);
    }

    @Step("Проверка видимости уведомлений и Ожидание, когда успешное уведомление пропадет")
    public void shouldBeExistAndWaitNotExist(String text) {
        shouldBeExist(text);
        waitNotExist(text);
    }
}