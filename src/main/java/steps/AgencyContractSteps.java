package steps;

import com.codeborne.selenide.Condition;
import io.qameta.allure.Step;
import page.AgencyContractPage;
import service.ConfProperties;

import java.io.File;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.openqa.selenium.Keys.ENTER;

public class AgencyContractSteps {

    @Step("Клик по кнопке '+ Добавить договор'")
    public void clickAddContractButton() {
        $(new AgencyContractPage().onAgencyContractForm().addContractButton).click();
    }

    @Step("Клик по кнопке 'Выбрать файл'")
    public void clickSelectFileButton() {
        $(new AgencyContractPage().onAgencyContractForm().selectFileButton).click();
    }

    @Step("Клик по кнопке 'Сохранить' для загруженных файлов")
    public void clickSaveFileButton() {
        $(new AgencyContractPage().onAgencyContractForm().saveFileButton).shouldBe(Condition.visible).click();
    }

    @Step("Добавление файла через input")
    public void inputFile(String file) {
        $(new AgencyContractPage().onAgencyContractForm().inputFile)
                .uploadFile(new File(ConfProperties.getProperty("web.uploadFilePlace") + file));
    }

    @Step("Проверка видимости добавленого файла в блоке 'Новый агентский договор'")
    public void shouldBeVisibleInputFile(String file){
        $$(new AgencyContractPage().onAgencyContractForm().addedFileNames).findBy(Condition.text(file)).shouldBe(Condition.visible);
    }

    @Step("Проверка количества добавленых файлов в блоке 'Новый агентский договор'")
    public void shouldHaveSizeInputFiles(int size){
        $$(new AgencyContractPage().onAgencyContractForm().addedFileNames).shouldHaveSize(size);
    }

    @Step("Добавление файла")
    public void addFile(String file){
        clickSelectFileButton();
        inputFile(file);
        shouldBeVisibleInputFile(file);
    }

    @Step("Удалить любой из добавленных файлов в блоке 'Новый агентский договор'")
    public void deleteAnyAddedFile(){
        $$(new AgencyContractPage().onAgencyContractForm().deleteFileButton).stream().findAny().get().click();
    }

    @Step("Получение данных поля: 'Номер агентского договора'")
    public String getContractNumber() {
        return $(new AgencyContractPage().onAgencyContractForm().contractNumberValue).getText();
    }

    @Step("Получение данных поля: 'Дата агентского договора'")
    public String getContractDate() {
        return $(new AgencyContractPage().onAgencyContractForm().contractDateValue).getText();
    }

    @Step("Заполнение формы 'Информация' данными: Номер агентского договора = {number}, Дата агентского договора = {date}")
    public void fillInformation(String number, String date) {
        $(new AgencyContractPage().onAgencyContractForm().contractNumberField).setValue(number);
        if ($(new AgencyContractPage().onAgencyContractForm().cleanFieldDateButton).isDisplayed()) {
            $(new AgencyContractPage().onAgencyContractForm().cleanFieldDateButton).click();
        }
        $(new AgencyContractPage().onAgencyContractForm().contractDateField).click();
        $(new AgencyContractPage().onAgencyContractForm().fieldDateActive).setValue(date);
        $(new AgencyContractPage().onAgencyContractForm().fieldDateActive).sendKeys(ENTER);
    }

    @Step("Клик кнопки 'Редактировать' в блоке 'Информация'")
    public void clickEditButton() {
        $(new AgencyContractPage().onAgencyContractForm().editButton).click();
    }

    @Step("Клик кнопки 'Отмена редактирования' в блоке 'Информация'")
    public void clickCancelButton() {
        $(new AgencyContractPage().onAgencyContractForm().cancelButton).click();
    }

    @Step("Клик кнопки 'Подтверждения редактирования' в блоке 'Информация'")
    public void clickAssentButton() {
        $(new AgencyContractPage().onAgencyContractForm().assentButton).click();
    }

    @Step("Проверка, что значение {newSome} равно {old}")
    public void assertThatEqualTo(String newSome, String old) {
        assertThat(newSome, equalTo(old));
    }
}