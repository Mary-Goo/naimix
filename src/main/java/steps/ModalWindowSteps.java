package steps;

import io.qameta.allure.Step;
import page.ModalWindowPage;

import static com.codeborne.selenide.Selenide.$;

public class ModalWindowSteps {
    @Step("Проверка что модальное окно отображается")
    private void shouldBeDisplayed() {
        $(new ModalWindowPage().onModalWindowForm().modalWindow).isDisplayed();
    }

    @Step("Клик по кнопке 'Да' в модальном окне")
    private void clickYesButton() {
        $(new ModalWindowPage().onModalWindowForm().yesButton).click();
    }

    @Step("Клик по кнопке 'Нет' в модальном окне")
    private void clickNoButton() {
        $(new ModalWindowPage().onModalWindowForm().noButton).click();
    }

    @Step("Проверка что модальное окно отображается и клик по кнопке {}")
    public void shouldBeDisplayedAndClickButton(String button) {
        shouldBeDisplayed();
        switch (button) {
            case ("Yes"):
                clickYesButton();
                break;
            case ("No"):
                clickNoButton();
                break;
            default:
                System.out.println("Не верно указано имя кнопки в модальном окне");
                break;
        }
    }
}