package steps;

import io.qameta.allure.Step;
import models.Employee;
import page.CompaniesPage;
import page.CompanyEmployeesPage;

import java.util.List;

import static com.codeborne.selenide.Selenide.$;

public class BeforeClassSteps {
    private LoginSteps loginSteps = new LoginSteps();
    private CompaniesSteps companiesSteps = new CompaniesSteps();
    private CompanyEmployeesSteps companyEmployeesSteps = new CompanyEmployeesSteps();
    private DocumentsSteps documentsSteps = new DocumentsSteps();
    private ToastsSteps toastsSteps = new ToastsSteps();
    private AgencyContractSteps agencyContractSteps = new AgencyContractSteps();
    private CompanySteps companySteps = new CompanySteps();
    private CreateSomethingByAdmin createSomething = new CreateSomethingByAdmin();
    private String nameCompany;

    @Step("Поиск компании для теста")
    public String findCompamyForTest() {
        String nameCompany;
        if (companiesSteps.findCompanyFromPageCount() == 0) {
            nameCompany = createSomething.createCompanyUL().getShortCompanyName();
            loginSteps.openTest();
            loginSteps.loginForAdmin("web.adminCompanyLogin", "web.adminCompanyPassword");
        } else {
            nameCompany = companiesSteps.choseAnyCompanyReturnName();
        }
        return nameCompany;
    }

    @Step("Поиск сотрудника в компании для теста")
    public Employee findEmployeeForTest(String nameCompany) {
        Employee employeeNew;
        if ($(new CompanyEmployeesPage().onCompanyEmployeesForm().countEmployee).getText() == "0") {
            employeeNew = createSomething.createEmployee(nameCompany);
            loginSteps.openTest();
            loginSteps.loginForAdmin("web.adminCompanyLogin", "web.adminCompanyPassword");
        } else {
            employeeNew = companyEmployeesSteps.choseEmployeeReturnEmployee();
        }
        return employeeNew;
    }

    @Step("Поиск компании для тестирования или создание новой")
    public String choseOrCreateCompanyUL() {
        String nameCompany;
        if (companiesSteps.findCompanyFromPageCount() == 0) {
            nameCompany = createSomething.createCompanyUL().getShortCompanyName();
            loginSteps.openTest();
            loginSteps.loginForAdmin("web.adminCompanyLogin", "web.adminCompanyPassword");
        } else {
            nameCompany = companiesSteps.choseAnyCompanyReturnName();
        }
        return nameCompany;
    }

    @Step("Поиск компании по ФИО или части ФИО {name} сотрудника")
    public void companyFilterEmployeeName(String name, boolean full) {
        $(new CompaniesPage().onCompaniesForm().clientUserFioFilter).setValue(name);
        companiesSteps.clickFindButton();
        List<String> сompanysNames = companiesSteps.getCompanyNames();
        сompanysNames.forEach(сompanyName -> {
            $(new CompaniesPage().onCompaniesForm().сompanyNameLink(сompanyName)).click();
            companySteps.openEmployeesPage();
            if (full) {
                companyEmployeesSteps.countEmployeesGreaterThanZero(name);
            } else {
                companyEmployeesSteps.countEmployeesGreaterThanZeroPart(name);
            }
        });
    }
}
