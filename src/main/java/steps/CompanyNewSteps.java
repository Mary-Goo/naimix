package steps;

import io.qameta.allure.Step;
import models.CompanyUL;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import page.CompanyNewPage;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static org.openqa.selenium.Keys.*;

public class CompanyNewSteps {

    @Step("Заполнение формы добавления новой компании 'Юридическое лицо'")
    public void fillAddCompanyUL(CompanyUL companyULData) {
        $(new CompanyNewPage().onCompanyNewForm().fullNameCompanyField).setValue(companyULData.getOfficialCompanyName());
        $(new CompanyNewPage().onCompanyNewForm().nameCompanyField).setValue(companyULData.getShortCompanyName());
        $$(new CompanyNewPage().onCompanyNewForm().categoryCompanyField).stream().findAny().get().click();
        $(new CompanyNewPage().onCompanyNewForm().innCompanyField).setValue(companyULData.getInn());
        $(new CompanyNewPage().onCompanyNewForm().addressIn).sendKeys(chord(Keys.CONTROL, "a"), DELETE);
        $(new CompanyNewPage().onCompanyNewForm().addressIn).setValue(companyULData.getActualAddress());
        $(new CompanyNewPage().onCompanyNewForm().addressChose).click();
    }

    @Step("Клик по кнопке добавить")
    public void clickApplyButton() {
        $(new CompanyNewPage().onCompanyNewForm().applyButton).click();
    }

    @Step("Выбор формы регистрации {}")
            public void choseRegistrationForm(String registrationForm) {
        $(new CompanyNewPage().onCompanyNewForm().fieldName).$("i").click();
        $(By.xpath("//div[@class='visible menu transition']//span[text()='"+registrationForm+"']")).click();
    }
}