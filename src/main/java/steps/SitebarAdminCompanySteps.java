package steps;

import io.qameta.allure.Step;
import page.SitebarAdminCompanyPage;

import static com.codeborne.selenide.Selenide.$;

public class SitebarAdminCompanySteps {

    @Step("Клик пункт меню 'Проекты'")
    public void openProjectPage(){
        $(new SitebarAdminCompanyPage().onSitebarAdmidCompanyForm().projectPage).click();
    }
}