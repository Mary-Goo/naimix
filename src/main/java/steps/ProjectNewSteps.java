package steps;

import io.qameta.allure.Step;
import models.Project;
import page.ProjectNewPage;
import service.RandomGenerate;

import static com.codeborne.selenide.Selenide.$;

public class ProjectNewSteps {
    private RandomGenerate randomGenerate = new RandomGenerate();

    @Step("Заполнение блоков 'Добавление проекта' и 'Параметры проекта'")
    public void fillAllForm(Project project) {
        fillNameProject(project.getName());
        fillBudgetProject(project.getBudget());
    }

    @Step("Заполнение блока 'Добавление проекта', поле 'Название проекта'")
    public void fillNameProject(String name) {
        $(new ProjectNewPage().onProjectNewForm().nameProjectField).setValue(name);
    }

    @Step("Заполнение блока 'Параметры проекта', поле 'Бюджет проекта'")
    public void fillBudgetProject(String val) {
        $(new ProjectNewPage().onProjectNewForm().amountValue).setValue(val);
    }

    @Step("Клик по кнопке 'Сохранить'")
    public void clickSaveButton() {
        $(new ProjectNewPage().onProjectNewForm().saveButton).click();
    }
}