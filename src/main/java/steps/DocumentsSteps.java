package steps;

import io.qameta.allure.Step;
import page.DocumentsPage;

import static com.codeborne.selenide.Selenide.$;

public class DocumentsSteps {
    @Step("Переход на вкладку 'Агенсткий договор' в Документах компании")
    public void clickAgencyContractPage(){
        $(new DocumentsPage().onDocumentsPage().agencyContractPage).click();
    }

}
