package steps;

import io.qameta.allure.Step;
import models.CompanyIP;
import models.CompanyUL;
import models.Employee;
import service.RandomInn;

public class CreateSomethingByAdmin {
    private LoginSteps loginSteps = new LoginSteps();
    private CompaniesSteps companiesSteps = new CompaniesSteps();
    private SitebarSteps sitebarSteps = new SitebarSteps();
    private ToastsSteps toastsSteps = new ToastsSteps();
    private CompanySteps companySteps = new CompanySteps();
    private CompanyNewSteps companyNewSteps = new CompanyNewSteps();
    private CompanyNewIPSteps companyNewIPSteps = new CompanyNewIPSteps();
    private RandomInn randomInn = new RandomInn();
    private CompanyEmployeesSteps companyEmployeesSteps = new CompanyEmployeesSteps();
    private EmployeeNewSteps employeeNewSteps = new EmployeeNewSteps();
    private String inn;

    @Step("Создание новой компании 'Юридическое лицо'")
    public CompanyUL createCompanyUL() {
        inn = randomInn.getInnUl();
        loginSteps.openTest();
        loginSteps.loginForAdmin("web.adminCompanyLogin", "web.adminCompanyPassword");
        CompanyUL newCompanyUL = new CompanyUL()
                .setBusinessRegistrationForm("Юридическое лицо")
                .setRandomOfficialCompanyName()
                .setRandomShortCompanyName()
                .setInn(inn)
                .setActualAddress("191036, г Санкт-Петербург, ул 6-я Советская");
        sitebarSteps.openCompaniesPage();
        companiesSteps.closeTasksList();
        companiesSteps.clickcreateCompanyButton();
        companyNewSteps.fillAddCompanyUL(newCompanyUL);
        companyNewSteps.clickApplyButton();
        toastsSteps.shouldBeExist("Компания включена");
        toastsSteps.shouldBeExist("Компания успешно добавлена");
        toastsSteps.waitNotExist("Компания включена");
        toastsSteps.waitNotExist("Компания успешно добавлена");
        loginSteps.loginOut();
        return newCompanyUL;
    }

    @Step("Создание новой компании 'Индивидуальный предприниматель'")
    public CompanyIP createCompanyIP() {
        inn = randomInn.getInnFl();
        loginSteps.openTest();
        loginSteps.loginForAdmin("web.adminCompanyLogin", "web.adminCompanyPassword");
        CompanyIP newCompanyIP = new CompanyIP()
                .setRandomFullNameIP()
                .setInn(inn)
                .setActualAddress("191036, г Санкт-Петербург, ул 6-я Советская");
        sitebarSteps.openCompaniesPage();
        companiesSteps.closeTasksList();
        companiesSteps.clickcreateCompanyButton();
        companyNewIPSteps.fillAddCompanyIP(newCompanyIP);
        companyNewSteps.clickApplyButton();
        toastsSteps.shouldBeExist("Компания включена");
        toastsSteps.shouldBeExist("Компания успешно добавлена");
        toastsSteps.waitNotExist("Компания включена");
        toastsSteps.waitNotExist("Компания успешно добавлена");
        loginSteps.loginOut();
        return newCompanyIP;
    }

    @Step("Создание нового сотрудника в компании")
    public Employee createEmployee(String shotNameCompany) {
        Employee newEmployee = new Employee().setAllFills();
        loginSteps.openTest();
        loginSteps.loginForAdmin("web.adminCompanyLogin", "web.adminCompanyPassword");
        companiesSteps.fillShotName(shotNameCompany);
        companiesSteps.clickFindButton();
        companiesSteps.openCompanyAtName(shotNameCompany);
        companySteps.openEmployeesPage();
        companyEmployeesSteps.addNewEmployee();
        employeeNewSteps.fillEmployeeNew(newEmployee);
        employeeNewSteps.clickAddButton();
        toastsSteps.shouldBeExistAndWaitNotExist("Сотрудник успешно добавлен");
        sitebarSteps.openCompaniesPage();
        loginSteps.loginOut();
        return newEmployee;
    }
}