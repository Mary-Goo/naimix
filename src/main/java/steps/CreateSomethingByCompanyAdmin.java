package steps;

import io.qameta.allure.Step;
import models.Project;

public class CreateSomethingByCompanyAdmin {
    private ProjectsSteps projectSteps = new ProjectsSteps();
    private ProjectNewSteps projectNewSteps = new ProjectNewSteps();
    private ToastsSteps toastsSteps = new ToastsSteps();

    @Step("Создание проекта без авторизации")
    public Project createProject() {
        Project project = new Project()
                .setNameRandom()
                .setBudget("10000")
                .setDescription("Описание проекта")
                .setStatus("В работе");
        projectSteps.clickCreateProjectButton();
        projectNewSteps.fillAllForm(project);
        projectNewSteps.clickSaveButton();
        toastsSteps.shouldBeExist("Проект успешно добавлен");
        toastsSteps.waitNotExist("Проект успешно добавлен");
        return project;
    }
}