package steps;

import io.qameta.allure.Step;
import page.SitebarPage;

import static com.codeborne.selenide.Selenide.$;

public class SitebarSteps {

    @Step ("Открыть экран 'Компании'")
    public void openCompaniesPage() {
        $(new SitebarPage().onSitebarForm().companiesPage).click();
    }
}