package steps;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import models.Employee;
import org.openqa.selenium.By;
import page.CompanyEmployeesPage;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Condition.exactOwnText;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.greaterThan;

public class CompanyEmployeesSteps {

    @Step("Клик по кнопке '+ Добавить сотрудника'")
    public void addNewEmployee() {
        $(new CompanyEmployeesPage().onCompanyEmployeesForm().addNewEmployeeButton).click();
    }

    @Step("Выбор любого сотрудника. Запись его данных")
    public Employee choseEmployeeReturnEmployee() {
        Employee employee;
        SelenideElement em = $$(new CompanyEmployeesPage().onCompanyEmployeesForm().allInfoFromEmployee).first();
        String[] fullName = (em.find(new CompanyEmployeesPage().onCompanyEmployeesForm().fullNameInEmployee).getText()).split("\\s");
        String email = em.find(new CompanyEmployeesPage().onCompanyEmployeesForm().emailInEmployee).getText();
        String phone = em.find(new CompanyEmployeesPage().onCompanyEmployeesForm().phoneInEmployee).getText()
                .replace("-", "").replaceAll("\\s", "").replace("(", "").replace(")", "").substring(2);
        employee = new Employee().setPhone(phone).setLastName(fullName[0]).setFirstName(fullName[1]).setPatronymic(fullName[2]).setEmail(email);
        return employee;
    }

    @Step("Поиск количества сотрудников с Email = {email} по всем страницам в компании")
    public int findEmployeesNumberByEmailOnAllPages(String email) {
        String finishPageS = $$(new CompanyEmployeesPage().onCompanyEmployeesForm().getPageNumber).last().getValue();
        assert finishPageS != null;
        int count = 0;
        for (int i = 1; i <= Integer.parseInt(finishPageS); i++) {
            sleep(3000);
            if ($$(new CompanyEmployeesPage().onCompanyEmployeesForm().getAllEmailOnPage).findBy(text(email)).isDisplayed()) {
                count = count + (int) $$(new CompanyEmployeesPage().onCompanyEmployeesForm().getAllEmailOnPage).filterBy(exactOwnText(email)).stream().count();
                refresh();
            } else if (i < Integer.parseInt(finishPageS)) {
                $(By.linkText("" + (i + 1))).click();
            }
        }
        return count;
    }

    @Step("Поиск количества сотрудников по полному ФИО в компании по страницам")
    public int countEmployeeOnPagesByName(String fullName) {
        String finishPageS = $$(new CompanyEmployeesPage().onCompanyEmployeesForm().getPageNumber).last().getValue();
        assert finishPageS != null;
        int count = 0;
        for (int i = 1; i <= Integer.parseInt(finishPageS); i++) {
            if ($$(new CompanyEmployeesPage().onCompanyEmployeesForm().fullNameEmployee).findBy(text(fullName)).isDisplayed()) {
                count = count + (int) $$(new CompanyEmployeesPage().onCompanyEmployeesForm().fullNameEmployee).filterBy(exactOwnText(fullName)).stream().count();
                refresh();
            } else if (i < Integer.parseInt(finishPageS)) {
                $(By.linkText("" + (i + 1))).click();
            }
        }
        return count;
    }

    @Step("Поиск количества сотрудников (до первого) по части ФИО в компании по страницам")
    public int countFirstEmployeeOnPagesByPartName(String partName) {
        String finishPageS = $$(new CompanyEmployeesPage().onCompanyEmployeesForm().getPageNumber).last().getValue();
        assert finishPageS != null;
        int count = 0;
        for (int i = 1; i <= Integer.parseInt(finishPageS); i++) {
            List<String> employeesNamesFromPage = getEmployeesNamesOnPage();
            for (String name : employeesNamesFromPage) {
                if (name.contains(partName)) {
                    count = count + 1;
                    return count;
                }
            }
        }
        return count;
    }

    @Step("Список имен, сотрудников на странице")
    public List<String> getEmployeesNamesOnPage() {
        List<String> employeesNames = new ArrayList<>();
        sleep(3000);
        List<SelenideElement> elements = $$(new CompanyEmployeesPage().onCompanyEmployeesForm().fullNameEmployee);
        for (SelenideElement element : elements) {
            String name = element.getText();
            employeesNames.add(name);
        }
        return employeesNames;
    }

    @Step("Проверка, что ФИО сотрудника содержит часть имени")
    private void nameEmployeesCheckName(String name, String checkName) {
        assertThat(name, containsStringIgnoringCase(checkName));
    }

    @Step("Проверка что сотрудников с почтой {email} стало {count} человек")
    public void checkEmployeesNumberByEmailEqualToInt(String email, int count) {
        refresh();
        assertThat(findEmployeesNumberByEmailOnAllPages(email), equalTo(count));
    }

    @Step("Проверка, что в компании найдно сотрудников с полным именем {fullName} более чем 0")
    public void countEmployeesGreaterThanZero(String fullName) {
        assertThat(countEmployeeOnPagesByName(fullName), greaterThan(0));
    }

    @Step("Проверка, что в компании найдно сотрудников с именем, содержащим {partName}, более чем 0")
    public void countEmployeesGreaterThanZeroPart(String partName) {
        assertThat(countFirstEmployeeOnPagesByPartName(partName), greaterThan(0));
    }
}