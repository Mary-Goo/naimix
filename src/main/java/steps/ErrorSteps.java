package steps;

import io.qameta.allure.Step;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class ErrorSteps {

    @Step("Проверка равенства текущего текста ошибки {currentError} для поля {name}, ожидаемой ошибке {expectedError}")
    public void assertThatTextErrorIsExpected(String name, String currentError, String expectedError) {
        assertThat(currentError, equalTo(expectedError));
    }
}