package steps;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import models.Project;
import org.openqa.selenium.By;
import page.ProjectsPage;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsStringIgnoringCase;

public class ProjectsSteps {
    private ToastsSteps toastsSteps = new ToastsSteps();
    private ModalWindowSteps modalWindowSteps = new ModalWindowSteps();
    private ProjectNewSteps projectNewSteps = new ProjectNewSteps();

    @Step("Клик по кнопке '+Создать проект'")
    public void clickCreateProjectButton() {
        $(new ProjectsPage().onProjectsForm().createProjectButton).click();
    }

    @Step("Получение списка проектов на странице. Сохранение параметров: Имя и Статус проекта")
    public List<Project> getProjectsOnOpenPage() {
        List<Project> projects = new ArrayList<>();
        List<SelenideElement> elements = $$(new ProjectsPage().onProjectsForm().allProjectsName);
        for (SelenideElement element : elements) {
            String name = element.find(By.xpath(".//a")).getOwnText();
            String status = element.find(new ProjectsPage().onProjectsForm().statusInProjectLine).getOwnText();
            Project project = new Project()
                    .setName(name)
                    .setStatus(status);
            projects.add(project);
        }
        return projects;
    }

    @Step("Получение списка имен проектов на странице")
    public List<String> getProjectsNameOnOpenPage() {
        return $$(new ProjectsPage().onProjectsForm().allProjectsName).texts();
    }

    @Step("Изменить статус проекта")
    public void changeProjectStatus(String nameProject) {
        choseWhatTodo("Закрыть проект", nameProject);
        modalWindowSteps.shouldBeDisplayedAndClickButton("Yes");
        toastsSteps.shouldBeExistAndWaitNotExist("Статус успешно обновлен");
    }

    @Step("Заполнение поля 'Название'")
    public void fillFilterName(String name) {
        $(new ProjectsPage().onProjectsForm().nameField).setValue(name);
    }

    @Step("Клик по кнопке 'Найти'")
    public void clickFindButton() {
        $(new ProjectsPage().onProjectsForm().findButton).click();
    }

    @Step("Клик по кнопке 'Очистить'")
    public void clickClearButton() {
        $(new ProjectsPage().onProjectsForm().clearButton).click();
    }

    @Step("Выбор статса {requiredValue} в фильтре")
    public void choseStatusInFilter(String requiredValue) {
        $(new ProjectsPage().onProjectsForm().openStatusMenu).click();
        $(new ProjectsPage().onProjectsForm().choseStatus(requiredValue)).click();
    }

    @Step("Выбор действия {doing} у проекта {nameProject}")
    public void choseWhatTodo(String doing, String nameProject) {
        $(new ProjectsPage().onProjectsForm().openToDoList(nameProject)).click();
        $(new ProjectsPage().onProjectsForm().choseWhatToDo(doing)).click();
    }

    @Step("Выбор проекта со статусом '{status}'. Если проекта нет, создание проекта")
    public Project chooseProjectWithStatus(String status) {
        Project project;
        List<Project> projects = getProjectsOnOpenPage().stream().filter(p -> p.getStatus().equals(status)).collect(Collectors.toList());
        if (projects.size() != 0) {
            project = projects.get(0);
        } else {
            project = createProject();
            projects.add(project);
            if (status == "Закрыт") {
                changeProjectStatus(project.getName());
            }
        }
        return project;
    }

    @Step("Подсчет количества проектов в статусе '{status}'")
    public int countProjectWithStatus(String status) {
        return getProjectsOnOpenPage().stream().filter(p -> p.getStatus().equals(status)).collect(Collectors.toList()).size();
    }

    @Step("Создание проекта без авторизации")
    public Project createProject() {
        Project project = new Project()
                .setNameRandom()
                .setBudget("10000")
                .setDescription("Описание проекта")
                .setStatus("В работе");
        clickCreateProjectButton();
        projectNewSteps.fillAllForm(project);
        projectNewSteps.clickSaveButton();
        toastsSteps.shouldBeExistAndWaitNotExist("Проект успешно добавлен");
        return project;
    }

    @Step("Проверка, что имя найденного проекта содержит имя искомого проекта")
    public void checkNameContainsCheckName(String name, String checkName) {
        assertThat(name, containsStringIgnoringCase(checkName));
    }
}