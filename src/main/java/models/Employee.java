package models;

import service.RandomGenerate;
import service.RandomInn;

public class Employee {
    private String lastName;
    private String firstName;
    private String patronymic;
    private String snils;
    private String innEmployee;
    private String position;
    private String phone;
    private String email;
    private String role;
    private String password;

    private RandomInn randomInn = new RandomInn();
    private RandomGenerate randomGenerate = new RandomGenerate();

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String getSnils() {
        return snils;
    }

    public String getInnEmployee() {
        return innEmployee;
    }

    public String getPosition() {
        return position;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getRole() {
        return role;
    }

    public String getPassword() {
        return password;
    }

    public Employee setAllFills() {
        String snils = randomInn.getSnils();
        String inn = randomInn.getInnFl();
        String password = randomGenerate.generatePassword();
        Employee employee = new Employee()
                .setRandomFirstName()
                .setRandomLastName()
                .setRandomPatronymic()
                .setRandomPhone()
                .setInnEmployee(inn)
                .setSnils(snils)
                .setPassword(password)
                .setRandomEmail();
        return employee;
    }

    public String generateEmail() {
        return randomGenerate.generatePassword() + "@autotest.com";
    }

    public Employee setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public Employee setRandomLastName() {
        String[] listLastName = {"Князев", "Беспалов", "Уваров", "Шашков", "Бобылёв", "Рожков", "Сысоев", "Селиверстов",
                "Иванко", "Петров", "Акуленко", "Кучкудуг", "Питерсон", "Пугачев", "Перегон", "Вейдер", "Степанко", "Куликов", "Мишустин"};
        int f = (int) (Math.random() * listLastName.length);
        this.lastName = listLastName[f];
        return this;
    }

    public Employee setRandomLastNameAtLength(int stringLength) {
        this.lastName = randomGenerate.randomString(stringLength);
        return this;
    }

    public Employee setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public Employee setRandomFirstName() {
        String[] listFirstName = {"Евгения", "Екатерина", "Елена", "Епистима",
                "Алексей", "Василий", "Влас", "Демьян", "Гордей", "Евгений", "Кузьма", "Макар", "Николай", "Прохор",
                "Виталий", "Виктор", "Дементий", "Кондрат", "Константин", "Игнат", "Максим", "Сергей",
                "Елисей", "Захар", "Илья", "Матвей", "Михаил", "Семен", "Фома"};
        int f = (int) (Math.random() * listFirstName.length);
        this.firstName = listFirstName[f];
        return this;
    }

    public Employee setRandomFirstNameAtLength(int stringLength) {
        this.firstName = randomGenerate.randomString(stringLength);
        return this;
    }

    public Employee setPatronymic(String patronymic) {
        this.patronymic = patronymic;
        return this;
    }

    public Employee setRandomPatronymic() {
        String[] listPatronymic = {"Павлович", "Петрович", "Платонович", "Робертович", "Романович", "Северинович", "Семенович",
                "Сергеевич", "Станиславович", "Степанович", "Тарасович", "Тимофеевич", "Федорович", "Феликсович", "Филиппович", "Эдуардович",
                "Юрьевич", "Яковлевич", "Ярославович"};
        int f = (int) (Math.random() * listPatronymic.length);
        this.patronymic = listPatronymic[f];
        return this;
    }

    public Employee setRandomPatronymicAtLength(int stringLength) {
        this.patronymic = randomGenerate.randomString(stringLength);
        return this;
    }

    public Employee setSnils(String snils) {
        this.snils = snils;
        return this;
    }

    public Employee setRandomSnilsAtLength(int length) {
        this.snils = randomGenerate.randomNumber(length);
        return this;
    }

    public Employee setInnEmployee(String inn) {
        this.innEmployee = inn;
        return this;
    }

    public Employee setRandomInnEmployeeAtLength(int length) {
        this.innEmployee = randomGenerate.randomNumber(length);
        return this;
    }

    public Employee setPosition(String position) {
        this.position = position;
        return this;
    }

    public Employee setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public Employee setRandomPhone() {
        this.phone = "903" + randomGenerate.randomNumber(7);
        return this;
    }

    public Employee setRandomPhoneAtLength(int length) {
        this.phone = "903" + randomGenerate.randomNumber(length - 4);
        return this;
    }

    public Employee setRandomPhoneString() {
        this.phone = randomGenerate.randomString(10);
        return this;
    }

    public Employee setEmail(String email) {
        this.email = email;
        return this;
    }

    public Employee setEmailAtLength(String length, String domain) {
        this.email = randomGenerate.randomString(Integer.parseInt(length)) + domain;
        return this;
    }

    public Employee setRandomEmail() {
        this.email = generateEmail();
        return this;
    }

    public Employee setRole(String role) {
        this.role = role;
        return this;
    }

    public Employee setPassword(String password) {
        this.password = password;
        return this;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", snils='" + snils + '\'' +
                ", innEmployee='" + innEmployee + '\'' +
                ", position='" + position + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", role='" + role + '\'' +
                ", password='" + password + '\'' +
                ", randomInn=" + randomInn +
                ", randomGenerate=" + randomGenerate +
                '}';
    }
}