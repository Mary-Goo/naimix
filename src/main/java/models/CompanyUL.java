package models;

import service.RandomGenerate;

import java.util.Objects;

public class CompanyUL {
    private String businessRegistrationForm;
    private String officialCompanyName;
    private String shortCompanyName;
    private String inn;
    private String actualAddress;
    private String representativeName;
    private String category;
    private double commission;

    private RandomGenerate randomGenerate = new RandomGenerate();

    public String getBusinessRegistrationForm() {
        return businessRegistrationForm;
    }

    public String getOfficialCompanyName() {
        return officialCompanyName;
    }

    public String getShortCompanyName() {
        return shortCompanyName;
    }

    public String getInn() {
        return inn;
    }

    public String getActualAddress() {
        return actualAddress;
    }

    public String getRepresentativeName() {
        return representativeName;
    }

    public String getCategory() {
        return category;
    }

    public double getCommission() {
        return commission;
    }

    public CompanyUL setBusinessRegistrationForm(String businessRegistrationForm) {
        this.businessRegistrationForm = businessRegistrationForm;
        return this;
    }

    public CompanyUL setOfficialCompanyName(String officialCompanyName) {
        this.officialCompanyName = officialCompanyName;
        return this;
    }

    public CompanyUL setRandomOfficialCompanyName() {
        this.officialCompanyName = randomGenerate.randomString(13);
        return this;
    }

    public CompanyUL setShortCompanyName(String shortCompanyName) {
        this.shortCompanyName = shortCompanyName;
        return this;
    }

    public CompanyUL setRandomShortCompanyName() {
        this.shortCompanyName = randomGenerate.randomString(7);
        return this;
    }

    public CompanyUL setInn(String inn) {
        this.inn = inn;
        return this;
    }

    public CompanyUL setActualAddress(String actualAddress) {
        this.actualAddress = actualAddress;
        return this;
    }

    public CompanyUL setRepresentativeName(String representativeName) {
        this.representativeName = representativeName;
        return this;
    }

    public CompanyUL setCategory(String category) {
        this.category = category;
        return this;
    }

    public CompanyUL setCommission(double commission) {
        this.commission = commission;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanyUL that = (CompanyUL) o;
        return inn == that.inn &&
                Double.compare(that.commission, commission) == 0 &&
                businessRegistrationForm.equals(that.businessRegistrationForm) &&
                officialCompanyName.equals(that.officialCompanyName) &&
                shortCompanyName.equals(that.shortCompanyName) &&
                actualAddress.equals(that.actualAddress) &&
                category.equals(that.category);
    }

    @Override
    public String toString() {
        return "Company{" +
                "businessRegistrationForm='" + businessRegistrationForm + '\'' +
                ", officialCompanyName='" + officialCompanyName + '\'' +
                ", shortCompanyName='" + shortCompanyName + '\'' +
                ", inn='" + inn + '\'' +
                ", actualAddress='" + actualAddress + '\'' +
                ", representativeName='" + representativeName + '\'' +
                ", category='" + category + '\'' +
                ", commission=" + commission +
                '}';
    }
}