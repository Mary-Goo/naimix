package models;

import service.RandomGenerate;

import java.util.Objects;

public class CompanyIP {
    private String businessRegistrationForm= "Индивидуальный предприниматель";
    private String fullNameIP;
    private String inn;
    private String actualAddress;
    private String category;
    private String lastName;
    private double commission;


    private RandomGenerate randomGenerate = new RandomGenerate();


    public String getBusinessRegistrationForm() {
        return businessRegistrationForm;
    }

    public String getLastName() {
        return lastName;
    }

    public String getInn() {
        return inn;
    }

    public String getActualAddress() {
        return actualAddress;
    }

    public String getCategory() {
        return category;
    }

    public double getCommission() {
        return commission;
    }

    public String getFullNameIP() {
        return fullNameIP;
       }

    public CompanyIP setInn(String inn) {
        this.inn = inn;
        return this;
    }

    public CompanyIP setActualAddress(String actualAddress) {
        this.actualAddress = actualAddress;
        return this;
    }

    public CompanyIP setCategory(String category) {
        this.category = category;
        return this;
    }

    public CompanyIP setCommission(double commission) {
        this.commission = commission;
        return this;
    }

    public CompanyIP setRandomFullNameIP() {
        this.fullNameIP = setLastName()+" "+randomGenerate.randomFirstName()+" "+randomGenerate.randomPatronymic();
        return this;
    }

    public String setLastName(){
        return this.lastName = randomGenerate.randomLastName();
           }

    @Override
    public String toString() {
        return "CompanyIP{" +
                "businessRegistrationForm='" + businessRegistrationForm + '\'' +
                ", fullName='" + fullNameIP + '\'' +
                ", inn='" + inn + '\'' +
                ", actualAddress='" + actualAddress + '\'' +
                ", category='" + category + '\'' +
                ", commission=" + commission +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanyIP companyIP = (CompanyIP) o;
        return Double.compare(companyIP.commission, commission) == 0 &&
                businessRegistrationForm.equals(companyIP.businessRegistrationForm) &&
                fullNameIP.equals(companyIP.fullNameIP) &&
                inn.equals(companyIP.inn) &&
                actualAddress.equals(companyIP.actualAddress) &&
                category.equals(companyIP.category);
    }
}