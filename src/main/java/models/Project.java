package models;

import service.RandomGenerate;

import java.util.Objects;

public class Project {
    private String name;
    private String budget;
    private String description;
    private String status;
    private RandomGenerate randomGenerate = new RandomGenerate();

    public String getName() {
        return name;
    }

    public Project setNameRandom() {
        this.name = randomGenerate.randomString(10);
        return this;
    }

    public Project setName(String name) {
        this.name = name;
        return this;
    }

    public String getBudget() {
        return budget;
    }

    public Project setBudget(String budget) {
        this.budget = budget;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Project setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public Project setStatus(String status) {
        this.status = status;
        return this;
    }

    @Override
    public String toString() {
        return "Project{" +
                "name='" + name + '\'' +
                ", budget='" + budget + '\'' +
                ", description='" + description + '\'' +
                ", status='" + status + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Project project = (Project) o;
        return name.equals(project.name) &&
                status.equals(project.status);
    }
}