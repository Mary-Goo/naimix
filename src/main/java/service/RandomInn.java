package service;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class RandomInn {
    private String url = ConfProperties.getProperty("web.randomInnUrl");
    private By innFlButton = By.id("gen_innfl");
    private By innUlButton = By.id("gen_innul");
    private By snilsButton = By.id("gen_snils");
    private By innFlField = By.id("innfl");
    private By innUlField = By.id("innul");
    private By snilsField = By.id("snils");

    public String getInnFl() {
        open(url);
        $(innFlButton).click();
        return $(innFlField).getValue();
    }

    public String getInnUl() {
        open(url);
        $(innUlButton).click();
        return $(innUlField).getValue();
    }

    public String getSnils() {
        open(url);
        $(snilsButton).click();
        return $(snilsField).getValue();
    }
}