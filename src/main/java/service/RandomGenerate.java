package service;

import org.apache.commons.lang.RandomStringUtils;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class RandomGenerate {

    public String randomNumber(int length) {
        String s = "0123456789";
        StringBuffer number = new StringBuffer();

        for (int i = 0; i < length; i++) {
            number.append(s.charAt(new Random().nextInt(s.length())));
        }
        return number.toString();
    }

    public String randomString(int stringLength) {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        Random random = new Random();
        StringBuilder buffer = new StringBuilder(stringLength);
        for (int i = 0; i < stringLength; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        return buffer.toString();
    }

    public String generatePassword() {
        String upperCaseLetters = RandomStringUtils.random(2, 65, 90, true, true);
        String lowerCaseLetters = RandomStringUtils.random(2, 97, 122, true, true);
        String numbers = RandomStringUtils.randomNumeric(2);

        String combinedChars = upperCaseLetters.concat(lowerCaseLetters)
                .concat(numbers);
        List<Character> pwdChars = combinedChars.chars()
                .mapToObj(c -> (char) c)
                .collect(Collectors.toList());
        String password = pwdChars.stream()
                .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
                .toString();
        return password;
    }

    public String randomLastName() {
        String[] listLastName = {"Князев", "Беспалов", "Уваров", "Шашков", "Бобылёв", "Рожков", "Сысоев", "Селиверстов",
                "Иванко", "Петров", "Акуленко", "Кучкудуг", "Питерсон", "Пугачев", "Перегон", "Вейдер", "Степанко", "Куликов", "Мишустин"};
        int f = (int) (Math.random() * listLastName.length);
                return listLastName[f];
    }

    public String randomFirstName() {
        String[] listFirstName = {"Евгения", "Екатерина", "Елена", "Епистима",
                "Алексей", "Василий", "Влас", "Демьян", "Гордей", "Евгений", "Кузьма", "Макар", "Николай", "Прохор",
                "Виталий", "Виктор", "Дементий", "Кондрат", "Константин", "Игнат", "Максим", "Сергей",
                "Елисей", "Захар", "Илья", "Матвей", "Михаил", "Семен", "Фома"};
        int f = (int) (Math.random() * listFirstName.length);
        return listFirstName[f];
    }

    public String randomPatronymic() {
        String[] listPatronymic = {"Павлович", "Петрович", "Платонович", "Робертович", "Романович", "Северинович", "Семенович",
                "Сергеевич", "Станиславович", "Степанович", "Тарасович", "Тимофеевич", "Федорович", "Феликсович", "Филиппович", "Эдуардович",
                "Юрьевич", "Яковлевич", "Ярославович"};
        int f = (int) (Math.random() * listPatronymic.length);
        return listPatronymic[f];
    }
}