package forms;

import org.openqa.selenium.By;

public class DropDownForm {
    public By nameField = By.xpath("//div[@name='clientType']");
    public By allValueList = By.xpath("//div[@class='visible menu transition']/div/span");
    public By activeValue = By.xpath("//div[@name='clientType']//div[@class='text']");
}