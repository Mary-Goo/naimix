package forms;

import org.openqa.selenium.By;

public class ProjectNewForm {
    public By nameProjectField = By.xpath("//input[@placeholder='Введите название проекта']");
    public By amountValue = By.xpath("//input[@name='amount']");
    public By saveButton = By.xpath("//span[@class='btn-save__text']");
}