package forms;

import org.openqa.selenium.By;

public class ModalWindowForm {
    public By modalWindow = By.xpath("//div[@class='ui small modal transition visible active']");
    public By yesButton = By.xpath("//button[@class='ui primary button']");
    public By noButton = By.xpath("//button[@class='ui button']");
}