package forms;

import org.openqa.selenium.By;

public class CompaniesForm {
    public By createCompanyButton = By.xpath("//div[@class='nm-button__text '][text()='Создать компанию']");
    public By findButton = By.xpath("//div[@class='filter-buttons__button-send']");
    public By shortCompanyNameFilter = By.name("nameSubstringFilter");
    public By clientUserFioFilter = By.name("clientUserFioFilter");
    public By clientUserPhoneFilter = By.name("clientUserPhoneFilter");
    public By clientUserEmailFilter = By.name("clientUserEmailFilter");
    public By allCompanyNames = By.xpath("//td[@data-label='Наименование']/a");
    public By allCompanies = By.xpath("//td[@data-label='Наименование']");
    public By companyName = By.xpath(".//td[@data-label='Наименование']");
    public By tasksList = By.xpath("//div[@class='task']//i[@class='close icon']");
    public By addressCompanyFilter = By.xpath("//tr[@class='middle aligned']//td[@data-label='Адрес']");
    public By addressCompany = By.xpath(".//td[@data-label='Адрес']");
    public By archiveButtonForOne = By.xpath("//tr[@class='middle aligned']//div[@class='nmx-btn nmx-btn-default nmx-btn-sm-size']");
    public By settingsCompany = By.xpath("//tr[@class='middle aligned']//i[@class='icon setting']");
    public By archiveButton = By.xpath("//div[text()='Архив']");
    public By wholeCompany = By.xpath("//tr[@class='middle aligned']");

    public By сompanyNameLink(String сompanyName) {
        return By.xpath("//*[text()='" + сompanyName + "']");
    }
}