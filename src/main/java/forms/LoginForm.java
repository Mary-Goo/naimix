package forms;

import org.openqa.selenium.By;

public class LoginForm {
    public By loginButton = By.cssSelector("button.ui.button.login-auth__auth.login-auth__auth_login");
    public By loginField = By.name("login");
    public By passwordField = By.name("password");
    public By loginOutButton = By.xpath("//div[@class='nmx-menu__exit']");
    public By cookie = By.xpath("//div[@class='login-cookie__button']");
}