package forms;

import org.openqa.selenium.By;

public class ProjectsForm {
    public By createProjectButton = By.xpath("//button[@class='ui button orders__btn_green projects-list__create']");
    public By findButton = By.xpath("//span[@class='filter-buttons__button-send-text']");
    public By clearButton = By.xpath("//div[@class='filter-buttons__button-clear']");
    public By nameField = By.xpath("//input[@placeholder='Введите название']");
    public By statusField = By.xpath("//div[@name='status']");
    public By openStatusMenu = By.xpath("//div[@name='status']/i");
    public By allProjectsName = By.xpath("//div[@class='row nmx-grid-table__row  ']");
    public By statusInProjectLine = By.xpath(".//div[@class='column nmx-grid-table__column ']/span");

    public By openToDoList(String nameProject) {
        return By.xpath("//a[text()='" + nameProject + "']/../..//i[@class='ellipsis vertical icon']");
    }

    public By choseWhatToDo(String doing) {
        return By.xpath("//div[@class='menu active transition visible']/div[text()='" + doing + "']");
    }

    public By choseStatus(String requiredValue) {
        return By.xpath("//div[@class='visible menu transition']/div/span[text()='" + requiredValue + "']");
    }
}