package forms;

import org.openqa.selenium.By;

public class CompanyNewForm {
    public By fullNameCompanyField = By.name("fullName");
    public By nameCompanyField = By.name("name");
    public By innCompanyField = By.name("inn");
    public By categoryCompanyField = By.xpath("//div[@name='categoryId']");
    public By applyButton = By.cssSelector("span.apply-buttons__content");
    public By addressIn = By.cssSelector("input.react-dadata__input");
    public By addressChose = By.xpath("//*[./div[@name='address']]/div/span");
    public By fieldName = By.xpath("//div[@name='clientType']");
    public By allValueList = By.xpath("//div[@class='visible menu transition']/div/span");
    public By activeValue = By.xpath("//div[@name='clientType']//div[@class='text']");
}