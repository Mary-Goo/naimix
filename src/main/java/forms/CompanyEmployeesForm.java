package forms;

import org.openqa.selenium.By;

public class CompanyEmployeesForm {
    public By addNewEmployeeButton = By.xpath("//button[@class='nm-button nm-button_size-xl nm-button_color-green client-member-list__add']");
    public By getAllEmailOnPage = By.xpath("//tr[@class='middle aligned']/td[@data-label='E-mail']");
    public By getPageNumber = By.xpath("//div[@aria-label='Pagination Navigation']/a[@type='pageItem']");
    public By countEmployee = By.xpath("//div[@class='nm-title__count']");
    public By allInfoFromEmployee = By.xpath("//tr[@class='middle aligned']");
    public By fullNameInEmployee =By.xpath("./td[@data-label='ФИО']");
    public By emailInEmployee= By.xpath("./td[@data-label='E-mail']");
    public By phoneInEmployee= By.xpath("./td[@data-label='Номер телефона']");
    public By fullNameEmployee=By.xpath("//td[@data-label='ФИО']");
}