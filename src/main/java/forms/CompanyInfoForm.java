package forms;

import org.openqa.selenium.By;

public class CompanyInfoForm {
    public By companyDetailsTitle = By.xpath("//div[text()='Реквизиты компании']");
    public By companyTitle = By.xpath("//div[@class='client-list-header']");
    public By errorText = By.xpath("//div[@class='ui pointing prompt label']");
    public By editButton = By.xpath("//div[@class='client-list-header']/i[@class='material-icons notranslate']");
    public By fieldTitleCompany = By.xpath("//input[@name='name']");
    public By cancelEditButton = By.xpath("//i[text()='clear']");
    public By applyEditButton = By.xpath("//i[text()='check']");
}