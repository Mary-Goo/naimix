package forms;

import org.openqa.selenium.By;

public class AgencyContractForm {
    public By addContractButton = By.xpath("//span[text()='Добавить договор']");
    public By selectFileButton = By.xpath("//button[@class='ui primary button nmx-btn nmx-btn-blue-filled ']");
    public By inputFile = By.xpath("//input[@type='file']");
    public By addedFileNames = By.className("document-element__text");
    public By saveFileButton = By.xpath("//span[@class='apply-buttons__content']");
    public By deleteFileButton = By.xpath("//i[@class='material-icons document-element__close-icon document-element__close-icon_gray notranslate']");
    public By contractNumberValue = By.xpath("//div[text()='Номер агентского договора']/../div[@class='label-text__content ']");
    public By contractDateValue = By.xpath("//div[text()='Дата агентского договора']/../div[@class='label-text__content ']");
    public By cleanFieldDateButton = By.xpath("//button[@class='react-datepicker__close-icon']");
    public By contractNumberField = By.xpath("//input[@name='nmContractNumber']");
    public By contractDateField = By.xpath("//div[@class='field nm-form-date-picker ']");
    public By fieldDateActive = By.xpath("//input[@class='react-datepicker-ignore-onclickoutside']");
    public By editButton = By.xpath("//i[@title='Редактировать']");
    public By cancelButton = By.xpath("//i[@title='Отмена']");
    public By assentButton = By.xpath("//div[@class='card-app  agency-contract-info']//i[@aria-hidden='true']");
}