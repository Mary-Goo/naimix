package forms;

import org.openqa.selenium.By;

public class EmployeeNewForm {
    public By lastNameField = By.name("lastName");
    public By firstNameField = By.name("firstName");
    public By patronymicField = By.name("patronymic");
    public By snilsField = By.name("snils");
    public By innField = By.name("inn");
    public By phoneField = By.name("phone");
    public By emailField = By.name("email");
    public By passwordField = By.name("password");
    public By repeatPasswordField = By.name("repeatPassword");
    public By anyDropDown = By.cssSelector("div.default.text");
    public By addButton = By.xpath("//button[@type='submit']");
    public By cancelButton = By.xpath("//button[@class='ui basic button nmx-btn nmx-btn-default add-margin-right']");
    public By snilsError = By.xpath("//input[@placeholder='СНИЛС']/../../div[@class='ui pointing prompt label']");
    public By innError = By.xpath("//input[@placeholder='ИНН']/../../div[@class='ui pointing prompt label']");
    public By emailError = By.xpath("//input[@name='email']/../../div[@class='ui pointing above prompt label']");

    public By errorTextValueUnderField(String fieldName) {
        return By.xpath("//input[@name='" + fieldName + "']/../../div[@class='ui pointing above prompt label']");
    }

    public By inputFieldByName(String fieldName) {
        return By.xpath("//input[@name='" + fieldName + "']");
    }
}